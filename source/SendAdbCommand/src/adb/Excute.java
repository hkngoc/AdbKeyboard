package adb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.codehaus.mojo.native2ascii.Native2Ascii;


public class Excute {

	public Excute() {
	}

	public static String run(String cmd) {
		String output = "";
		try {
			Process process = Runtime.getRuntime().exec(cmd);
			process.waitFor();

			InputStream stream = process.getInputStream();
			InputStreamReader streamReader = new InputStreamReader(stream);
			BufferedReader reader = new BufferedReader(streamReader);
			String line = null;

			line = reader.readLine();
			while (line != null) {
				output += line;
				line = reader.readLine();
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		} catch (InterruptedException e) {
			System.out.println(e.toString());
		}
		return output;
	}

	public static String convertCharacter(String input) {
		String output = "";
		output = Native2Ascii.nativeToAscii(input);
		return output;
	}
}
