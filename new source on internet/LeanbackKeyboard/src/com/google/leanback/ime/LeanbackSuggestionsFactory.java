package com.google.leanback.ime;

import android.inputmethodservice.InputMethodService;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import java.util.ArrayList;

public class LeanbackSuggestionsFactory {
	private static final boolean DEBUG = Log.isLoggable("LbSuggestionsFactory",	2);
	private static final int MODE_AUTO_COMPLETE = 2;
	private static final int MODE_DEFAULT = 0;
	private static final int MODE_DOMAIN = 1;
	private static final String TAG = "LbSuggestionsFactory";
	private final StringBuilder mComposing = new StringBuilder();
	private InputMethodService mContext;
	private int mCursorPositionInWord;
	private boolean mDomainStarted = false;
	private int mMode;
	private int mNumSuggestions;
	private final ArrayList<String> mSuggestions = new ArrayList<String>();

	public LeanbackSuggestionsFactory(
			InputMethodService paramInputMethodService, int paramInt) {
		this.mContext = paramInputMethodService;
		this.mNumSuggestions = paramInt;
		resetComposing();
	}

	private void addCharacterToComposing(CharSequence paramCharSequence) {
		this.mComposing.insert(this.mCursorPositionInWord, paramCharSequence);
		this.mCursorPositionInWord += paramCharSequence.length();
	}

	private void resetComposing() {
		resetComposing(true);
	}

	private void resetComposing(boolean paramBoolean) {
		this.mComposing.setLength(0);
		this.mCursorPositionInWord = this.mComposing.length();
		this.mSuggestions.clear();
		if ((paramBoolean) && (this.mMode == 2)) {
			updateComposingWordAndCursorPosition();
		}
	}

	private void updateComposingWordAndCursorPosition() {
		
//	    InputConnection localInputConnection = this.mContext.getCurrentInputConnection();
//	    CharSequence localCharSequence1 = localInputConnection.getTextAfterCursor(1000, 0);
//	    int i;
//	    CharSequence localCharSequence2;
//	    if (localCharSequence1 != null) {
//	    	i = localCharSequence1.length();
//	    	localCharSequence2 = localInputConnection.getTextBeforeCursor(1000, 0);
//	    	if (localCharSequence2 == null) {
//	    		break label121;
//	    	}
//	    }
//	    int k;
//	    label121:
//    	for (int j = localCharSequence2.length();; j = 0) {
//    		this.mDomainStarted = false;
//    		k = 0;
//    		if (localCharSequence1 == null) {
//    			break label127;
//    		}
//    		int i2 = localCharSequence1.length();
//    		k = 0;
//    		if (i2 == 0) {
//    			break label127;
//    		}
//    		for (int i3 = 0; (i3 < i) && (LeanbackUtils.isAlphabet(localCharSequence1.charAt(i3))); i3++) {
//    			k++;
//    		}
//    		i = 0;
//    		break;
//    	}
//	    label127:
//    	int m = 0;
//    	int i1;
//    	if (localCharSequence2 != null) {
//    		int n = localCharSequence2.length();
//    		m = 0;
//    		if (n != 0) {
//    			i1 = j - 1;
//    			while (i1 >= 0) {
//    				if (this.mMode != 1) {
//    					break label364;
//    				}
//    				if (localCharSequence2.charAt(i1) != '@') {
//    					m++;
//    					i1--;
//    				} else {
//    					this.mDomainStarted = true;
//    					m++;
//    				}
//    			}
//    		}
//    	}
//    	label202:
//		this.mComposing.setLength(0);
//    	if ((this.mMode == 1) && (!this.mDomainStarted)) {
//    		this.mComposing.append('@');
//    	}
//    
//    	for (this.mCursorPositionInWord = 0;; this.mCursorPositionInWord = m) {
//    		if ((m == 0) && (k == 0)) {
//    			resetComposing(false);
//    		}
//    		
//    		if (DEBUG) {
//	        Log.d("LbSuggestionsFactory", "textRight is " + localCharSequence1 + " right:" + k);
//	        Log.d("LbSuggestionsFactory", "textLeft is " + localCharSequence2 + " left:" + m);
//	        Log.d("LbSuggestionsFactory", "composing is " + this.mComposing.toString());
//    		}
//    		return;
//    		label364:
//			if ((this.mMode != 2) && (!LeanbackUtils.isAlphabet(localCharSequence2.charAt(i1)))) {
//				break label202;
//			}
//    		m++;
//    		break;
//	      if (localCharSequence2 != null) {
//	        this.mComposing.append(localCharSequence2.subSequence(j - m, j));
//	      }
//	      if (localCharSequence1 != null) {
//	        this.mComposing.append(localCharSequence1.subSequence(0, k));
//	      }
//    	}
	}

	private void updateSuggestions() {
		this.mSuggestions.clear();
	}

	public String getComposingString() {
		return this.mComposing.toString();
	}

	public int getCursorPosition() {
		return this.mCursorPositionInWord;
	}

	public ArrayList<String> getSuggestions() {
		return this.mSuggestions;
	}

	public void onDisplayCompletions(CompletionInfo[] paramArrayOfCompletionInfo) {
		if ((paramArrayOfCompletionInfo == null)
				|| (paramArrayOfCompletionInfo.length <= 0)) {
			return;
		}
		updateSuggestions();
		int i = paramArrayOfCompletionInfo.length;
		for (int j = 0;; j++) {
			if ((j >= i)
					|| (this.mSuggestions.size() >= this.mNumSuggestions)
					|| (TextUtils.isEmpty(paramArrayOfCompletionInfo[j]
							.getText()))) {
				if (!Log.isLoggable("LbSuggestionsFactory", 2)) {
					break;
				}
				for (int k = 0; k < this.mSuggestions.size(); k++) {
					Log.d("LbSuggestionsFactory", "completion " + k + ": "
							+ (String) this.mSuggestions.get(k));
				}
				break;
			}
			this.mSuggestions.add(j, paramArrayOfCompletionInfo[j].getText()
					.toString());
		}
	}

	public void onKey(int paramInt1, int paramInt2,
			CharSequence paramCharSequence, String paramString) {
		switch (paramInt1) {
		
		case 1:
			break;
		case 3:
			break;
		case 4:
			updateComposingWordAndCursorPosition();
			updateSuggestions();
			break;
		case 0:
			addCharacterToComposing(paramCharSequence);
			updateComposingWordAndCursorPosition();
			updateSuggestions();
			break;
		default:
			break;
		}
		resetComposing();
		updateSuggestions();
	}

	public void onStartInput(EditorInfo paramEditorInfo) {
		this.mMode = 0;
		if ((LeanbackUtils.getInputTypeClass(paramEditorInfo) == 1)
				&& (LeanbackUtils.getInputTypeVariation(paramEditorInfo) == 32)) {
			this.mMode = 1;
			this.mDomainStarted = false;
		} else {
			if ((0x10000 & paramEditorInfo.inputType) != 0) {
				this.mMode = 2;
			}
		}
		resetComposing();
	}
}
