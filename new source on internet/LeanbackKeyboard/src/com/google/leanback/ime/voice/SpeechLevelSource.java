package com.google.leanback.ime.voice;

public class SpeechLevelSource {
	private volatile int mSpeechLevel;

	public int getSpeechLevel() {
		return this.mSpeechLevel;
	}

	public boolean isValid() {
		return this.mSpeechLevel > 0;
	}

	public void reset() {
		this.mSpeechLevel = -1;
	}

	public void setSpeechLevel(int paramInt) {
		if ((paramInt < 0) || (paramInt > 100)) {
			throw new IllegalArgumentException();
		}
		this.mSpeechLevel = paramInt;
	}
}
