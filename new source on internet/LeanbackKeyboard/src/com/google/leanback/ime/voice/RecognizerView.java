package com.google.leanback.ime.voice;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.BaseSavedState;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class RecognizerView extends RelativeLayout {

	private static final boolean DEBUG = false;
	private static final String TAG = "RecognizerView";
	private Callback mCallback;
	private boolean mEnabled;
	protected ImageView mMicButton;
	private BitmapSoundLevelView mSoundLevels;
	private State mState;

	public RecognizerView(Context paramContext) {
		super(paramContext);
	}

	public RecognizerView(Context paramContext, AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
	}

	public RecognizerView(Context paramContext, AttributeSet paramAttributeSet,
			int paramInt) {
		super(paramContext, paramAttributeSet, paramInt);
	}

	private void updateState(State paramState) {
		this.mState = paramState;
		refreshUi();
	}

	public View getMicButton() {
		return this.mMicButton;
	}

	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		refreshUi();
	}

	public void onClick() {
		switch (this.mState) {
		case MIC_INITIALIZING:
			this.mCallback.onCancelRecordingClicked();
			break;
		case LISTENING:
			this.mCallback.onStopRecordingClicked();
			break;
		case NOT_LISTENING:
			this.mCallback.onCancelRecordingClicked();
			break;
		default:
			this.mCallback.onStartRecordingClicked();
			break;
		}

	}

	public void onFinishInflate() {
		LayoutInflater.from(getContext()).inflate(2130903044, this, true);
		this.mSoundLevels = ((BitmapSoundLevelView) findViewById(2131558406));
		this.mMicButton = ((ImageView) findViewById(2131558407));
		 this.mState = State.NOT_LISTENING;
	}

	public void onRestoreInstanceState(Parcelable paramParcelable) {
		if (!(paramParcelable instanceof SavedState)) {
			super.onRestoreInstanceState(paramParcelable);
			return;
		}
		SavedState localSavedState = (SavedState) paramParcelable;
		super.onRestoreInstanceState(localSavedState.getSuperState());
		this.mState = localSavedState.mState;
	}

	public Parcelable onSaveInstanceState() {
		SavedState localSavedState = new SavedState(super.onSaveInstanceState());
		localSavedState.mState = this.mState;
		return localSavedState;
	}

	protected void refreshUi() {
		if (!this.mEnabled) {
			return;
		}
		// switch (this.mState)
		// {
		// default:
		// return;
		// case ???:
		// this.mMicButton.setImageResource(2130837530);
		// this.mSoundLevels.setEnabled(false);
		// return;
		// case ???:
		// this.mMicButton.setImageResource(2130837530);
		// this.mSoundLevels.setEnabled(true);
		// return;
		// case ???:
		// this.mMicButton.setImageResource(2130837531);
		// this.mSoundLevels.setEnabled(true);
		// return;
		// case ???:
		// this.mMicButton.setImageResource(2130837529);
		// this.mSoundLevels.setEnabled(false);
		// return;
		// }
		this.mMicButton.setImageResource(2130837529);
		this.mSoundLevels.setEnabled(false);
	}

	public void setCallback(Callback paramCallback) {
		this.mCallback = paramCallback;
	}

	public void setMicEnabled(boolean paramBoolean) {
		this.mEnabled = paramBoolean;
		if (paramBoolean) {
			this.mMicButton.setAlpha(1.0F);
			this.mMicButton.setImageResource(2130837519);
			return;
		}
		this.mMicButton.setAlpha(0.1F);
		this.mMicButton.setImageResource(2130837521);
	}

	public void setMicFocused(boolean paramBoolean) {
		if (this.mEnabled) {
			if (paramBoolean) {
				this.mMicButton.setImageResource(2130837520);
			}
		} else {
			return;
		}
		this.mMicButton.setImageResource(2130837519);
	}

	public void setSpeechLevelSource(SpeechLevelSource paramSpeechLevelSource) {
		this.mSoundLevels.setLevelSource(paramSpeechLevelSource);
	}

	public void showInitializingMic() {
		updateState(State.MIC_INITIALIZING);
	}

	public void showListening() {
		updateState(State.LISTENING);
	}

	public void showNotListening() {
		updateState(State.NOT_LISTENING);
	}

	public void showRecognizing() {
		updateState(State.RECOGNIZING);
	}

	public void showRecording() {
		updateState(State.RECORDING);
	}

	public static abstract interface Callback {
		public abstract void onCancelRecordingClicked();

		public abstract void onStartRecordingClicked();

		public abstract void onStopRecordingClicked();
	}

	public static class SavedState extends View.BaseSavedState {
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public RecognizerView.SavedState createFromParcel(
					Parcel paramAnonymousParcel) {
				return new RecognizerView.SavedState(paramAnonymousParcel);
			}

			public RecognizerView.SavedState[] newArray(int paramAnonymousInt) {
				return new RecognizerView.SavedState[paramAnonymousInt];
			}
		};
		RecognizerView.State mState;

		
		public SavedState(Parcel paramParcel) {
			super(paramParcel);
			this.mState = RecognizerView.State
					.valueOf(paramParcel.readString());
		}

		public SavedState(Parcelable paramParcelable) {
			super(paramParcelable);
		}

		public void writeToParcel(Parcel paramParcel, int paramInt) {
			super.writeToParcel(paramParcel, paramInt);
			paramParcel.writeString(this.mState.toString());
		}
	}

	private static enum State {
		NOT_LISTENING, MIC_INITIALIZING, LISTENING, RECORDING, RECOGNIZING
	}
}
