package com.google.leanback.ime.voice;

import android.animation.TimeAnimator;
import android.animation.TimeAnimator.TimeListener;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.google.leanback.ime.R.styleable;

public class BitmapSoundLevelView
  extends View
{
  private static final boolean DEBUG = false;
  private static final int MIC_LEVEL_GUIDELINE_OFFSET = 13;
  private static final int MIC_PRIMARY_LEVEL_IMAGE_OFFSET = 3;
  private static final String TAG = "BitmapSoundLevelsView";
  private TimeAnimator mAnimator;
  private final int mCenterTranslationX;
  private final int mCenterTranslationY;
  private int mCurrentVolume;
  private Rect mDestRect;
  private final int mDisableBackgroundColor;
  private final Paint mEmptyPaint = new Paint();
  private final int mEnableBackgroundColor;
  private SpeechLevelSource mLevelSource;
  private final int mMinimumLevelSize;
  private Paint mPaint;
  private int mPeakLevel;
  private int mPeakLevelCountDown;
  private final Bitmap mPrimaryLevel;
  private final Bitmap mTrailLevel;
  
  public BitmapSoundLevelView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public BitmapSoundLevelView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public BitmapSoundLevelView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.BitmapSoundLevelView, paramInt, 0);
    this.mEnableBackgroundColor = localTypedArray.getColor(0, Color.parseColor("#66FFFFFF"));
    this.mDisableBackgroundColor = localTypedArray.getColor(1, -1);
    boolean bool1 = localTypedArray.hasValue(2);
    int i = 0;
    int j = 0;
    if (bool1)
    {
      j = localTypedArray.getResourceId(2, 2130837532);
      i = 1;
    }
    boolean bool2 = localTypedArray.hasValue(3);
    int k = 0;
    int m = 0;
    if (bool2)
    {
      m = localTypedArray.getResourceId(3, 2130837533);
      k = 1;
    }
    this.mCenterTranslationX = localTypedArray.getDimensionPixelOffset(5, 0);
    this.mCenterTranslationY = localTypedArray.getDimensionPixelOffset(6, 0);
    this.mMinimumLevelSize = localTypedArray.getDimensionPixelOffset(4, 0);
    localTypedArray.recycle();
    if (i != 0)
    {
      this.mPrimaryLevel = BitmapFactory.decodeResource(getResources(), j);
      if (k == 0) {
        break label287;
      }
    }
    label287:
    for (this.mTrailLevel = BitmapFactory.decodeResource(getResources(), m);; this.mTrailLevel = null)
    {
      this.mPaint = new Paint();
      this.mDestRect = new Rect();
      this.mEmptyPaint.setFilterBitmap(true);
      this.mLevelSource = new SpeechLevelSource();
      this.mLevelSource.setSpeechLevel(0);
      this.mAnimator = new TimeAnimator();
      this.mAnimator.setRepeatCount(-1);
      this.mAnimator.setTimeListener(new TimeAnimator.TimeListener()
      {
        public void onTimeUpdate(TimeAnimator paramAnonymousTimeAnimator, long paramAnonymousLong1, long paramAnonymousLong2)
        {
          BitmapSoundLevelView.this.invalidate();
        }
      });
      return;
      this.mPrimaryLevel = null;
      break;
    }
  }
  
  private void startAnimator()
  {
    if (!this.mAnimator.isStarted()) {
      this.mAnimator.start();
    }
  }
  
  private void stopAnimator()
  {
    this.mAnimator.cancel();
  }
  
  private void updateAnimatorState()
  {
    if (isEnabled())
    {
      startAnimator();
      return;
    }
    stopAnimator();
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    updateAnimatorState();
  }
  
  protected void onDetachedFromWindow()
  {
    stopAnimator();
    super.onDetachedFromWindow();
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    if (isEnabled())
    {
      paramCanvas.drawColor(this.mEnableBackgroundColor);
      int i = this.mLevelSource.getSpeechLevel();
      if (i > this.mPeakLevel)
      {
        this.mPeakLevel = i;
        this.mPeakLevelCountDown = 25;
        if (i <= this.mCurrentVolume) {
          break label376;
        }
      }
      label376:
      for (this.mCurrentVolume += (i - this.mCurrentVolume) / 4;; this.mCurrentVolume = ((int)(0.95F * this.mCurrentVolume)))
      {
        int j = this.mCenterTranslationX + getWidth() / 2;
        int k = this.mCenterTranslationY + getWidth() / 2;
        if (this.mTrailLevel != null)
        {
          int n = (j - this.mMinimumLevelSize) * this.mPeakLevel / 100 + this.mMinimumLevelSize;
          this.mDestRect.set(j - n, k - n, j + n, k + n);
          paramCanvas.drawBitmap(this.mTrailLevel, null, this.mDestRect, this.mEmptyPaint);
        }
        if (this.mPrimaryLevel != null)
        {
          int m = (j - this.mMinimumLevelSize) * this.mCurrentVolume / 100 + this.mMinimumLevelSize;
          this.mDestRect.set(j - m, k - m, j + m, k + m);
          paramCanvas.drawBitmap(this.mPrimaryLevel, null, this.mDestRect, this.mEmptyPaint);
          this.mPaint.setColor(getResources().getColor(2131165185));
          this.mPaint.setStyle(Paint.Style.FILL);
          paramCanvas.drawCircle(j, k, -3 + this.mMinimumLevelSize, this.mPaint);
        }
        if ((this.mTrailLevel != null) && (this.mPrimaryLevel != null))
        {
          this.mPaint.setColor(getResources().getColor(2131165184));
          this.mPaint.setStyle(Paint.Style.STROKE);
          paramCanvas.drawCircle(j, k, j - 13, this.mPaint);
        }
        return;
        if (this.mPeakLevelCountDown == 0)
        {
          this.mPeakLevel = Math.max(0, -2 + this.mPeakLevel);
          break;
        }
        this.mPeakLevelCountDown = (-1 + this.mPeakLevelCountDown);
        break;
      }
    }
    paramCanvas.drawColor(this.mDisableBackgroundColor);
  }
  
  public void onWindowFocusChanged(boolean paramBoolean)
  {
    super.onWindowFocusChanged(paramBoolean);
    if (paramBoolean)
    {
      updateAnimatorState();
      return;
    }
    stopAnimator();
  }
  
  public void setEnabled(boolean paramBoolean)
  {
    super.setEnabled(paramBoolean);
    updateAnimatorState();
  }
  
  public void setLevelSource(SpeechLevelSource paramSpeechLevelSource)
  {
    this.mLevelSource = paramSpeechLevelSource;
  }
}


/* Location:              C:\Users\Administrator\Desktop\Leanback_Keyboard_L-1236599_dex2jar.jar!\com\google\leanback\ime\voice\BitmapSoundLevelView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */