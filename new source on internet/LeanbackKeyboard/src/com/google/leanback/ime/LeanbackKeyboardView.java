package com.google.leanback.ime;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.media.AudioManager;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.List;

public class LeanbackKeyboardView extends FrameLayout {
	public static final int ASCII_PERIOD = 46;
	public static final int ASCII_SPACE = 32;
	@SuppressWarnings("unused")
	private static final boolean DEBUG = false;
	public static final int KEYCODE_CAPS_LOCK = 297;
	public static final int KEYCODE_DISMISS_MINI_KEYBOARD = 296;
	public static final int KEYCODE_LEFT = 295;
	public static final int KEYCODE_RIGHT = 294;
	public static final int KEYCODE_VOICE = 0;
	@SuppressWarnings("unused")
	private static final int NOT_A_KEY = -1;
	public static final int SHIFT_LOCKED = 2;
	public static final int SHIFT_OFF = 0;
	public static final int SHIFT_ON = 1;
	@SuppressWarnings("unused")
	private static final String TAG = "LbKbView";
	private AccessibilityManager mAccessibilityManager;
	private AudioManager mAudioManager;
	private final int mClickAnimDur;
	private final float mClickedScale;
	private int mColCount;
	private View mCurrentFocusView;
	private boolean mFocusClicked;
	private int mFocusIndex;
	private final float mFocusedScale;
	private boolean mHeadsetRequiredToHearPasswordsAnnounced;
	private ImageView[] mKeyImageViews;
	private int mKeyTextColor;
	private int mKeyTextSize;
	private Keyboard mKeyboard;
	private Keyboard.Key[] mKeys;
	private int mLabelTextSize;
	private boolean mMiniKeyboardOnScreen;
	private int mModeChangeTextSize;
	private Rect mPadding;
	private int mPaddingLeft;
	private int mPaddingRight;
	private int mPaddingTop;
	private int mPaddingBottom;
	private Paint mPaint;
	private int mRowCount;
	private int mShiftState;
	private final int mUnfocusStartDelay;
	private Context mContext;

	public LeanbackKeyboardView(Context paramContext,
			AttributeSet paramAttributeSet) {
		super(paramContext, paramAttributeSet);
		Resources localResources = paramContext.getResources();
		TypedArray localTypedArray = paramContext.getTheme()
				.obtainStyledAttributes(paramAttributeSet,
						R.styleable.LeanbackKeyboardView, 0, 0);
		this.mContext = paramContext;
		this.mRowCount = localTypedArray.getInteger(0, -1);
		this.mColCount = localTypedArray.getInteger(1, -1);
		this.mKeyTextSize = ((int) localResources.getDimension(2131296285));
		this.mPaint = new Paint();
		this.mPaint.setAntiAlias(true);
		this.mPaint.setTextSize(this.mKeyTextSize);
		this.mPaint.setTextAlign(Paint.Align.CENTER);
		this.mPaint.setAlpha(255);
		this.mPadding = new Rect(0, 0, 0, 0);
		this.mLabelTextSize = ((int) localResources.getDimension(2131296286));
		this.mModeChangeTextSize = ((int) localResources
				.getDimension(2131296287));
		this.mKeyTextColor = localResources.getColor(2131165187);
		this.mFocusIndex = -1;
		// this.mAccessibilityManager =
		// AccessibilityManager.getInstance(paramContext);
		this.mAudioManager = ((AudioManager) paramContext
				.getSystemService("audio"));
		this.mShiftState = 0;
		this.mFocusedScale = localResources.getFraction(2131230721, 1, 1);
		this.mClickedScale = localResources.getFraction(2131230720, 1, 1);
		this.mClickAnimDur = localResources.getInteger(2131361792);
		this.mUnfocusStartDelay = localResources.getInteger(2131361793);
	}

	private CharSequence adjustCase(CharSequence paramCharSequence) {
		if ((this.mKeyboard.isShifted()) && (paramCharSequence != null)
				&& (paramCharSequence.length() < 3)
				&& (Character.isLowerCase(paramCharSequence.charAt(0)))) {
			paramCharSequence = paramCharSequence.toString().toUpperCase();
		}
		return paramCharSequence;
	}

	private ImageView createKeyImageView(int paramInt) {
		Rect localRect = this.mPadding;
		int i = this.mPaddingLeft;
		int j = this.mPaddingTop;
		Keyboard.Key localKey = this.mKeys[paramInt];
		String str;
		Bitmap localBitmap = null;
		Canvas localCanvas = null;
		Paint localPaint = null;

		localBitmap = Bitmap.createBitmap(localKey.width, localKey.height,
				Bitmap.Config.ARGB_8888);
		localCanvas = new Canvas(localBitmap);
		localPaint = this.mPaint;
		localPaint.setColor(this.mKeyTextColor);

		if (localKey.label == null) {
			localCanvas.drawARGB(0, 0, 0, 0);
			ImageView localImageView = new ImageView(getContext());
			localImageView.setImageBitmap(localBitmap);
			addView(localImageView, new ViewGroup.LayoutParams(-2, -2));
			localImageView.setX(i + localKey.x);
			localImageView.setY(j + localKey.y);
			localImageView.setVisibility(0);

			return localImageView;
		} else {
			if (localKey.icon == null) {
				str = adjustCase(localKey.label).toString();

				if (str.length() <= 1) {
					if ((str.equals("&")) || (str.equals("@"))) {
						localPaint.setTextSize(this.mLabelTextSize);
						localPaint
								.setTypeface(Typeface.create("sans-serif", 0));
					} else {
						localPaint.setTextSize(this.mKeyTextSize);
						localPaint.setTypeface(Typeface.create(
								"sans-serif-light", 0));
					}
				} else {
					localPaint.setTextSize(this.mModeChangeTextSize);
					localPaint.setTypeface(Typeface.create("sans-serif", 0));
				}

				localCanvas.drawText(
						str,
						(localKey.width - localRect.left - localRect.right) / 2
								+ localRect.left,
						(localKey.height - localRect.top - localRect.bottom)
								/ 2
								+ (localPaint.getTextSize() - localPaint
										.descent()) / 2.0F + localRect.top,
						localPaint);
				localPaint.setShadowLayer(0.0F, 0.0F, 0.0F, 0);
			} else {
				if (localKey.codes[0] == -1) {
					if (this.mShiftState == 0) {
						/**
						 * shift off
						 */
						localKey.icon = this.mContext.getResources()
								.getDrawable(2130837514);
					} else if (this.mShiftState == 1) {
						/**
						 * shift on
						 */
						localKey.icon = this.mContext.getResources()
								.getDrawable(2130837515);
					} else {
						/**
						 * shift lock on
						 */
						localKey.icon = this.mContext.getResources()
								.getDrawable(2130837513);
					}
				} else {
					/**
					 * - delete
					 * - left arrow
					 * - right arrow
					 * - space
					 * - symbol
					 * - voice, mic
					 */
				}
				
				int k = (localKey.width - localRect.left - localRect.right - localKey.icon
						.getIntrinsicWidth()) / 2 + localRect.left;
				int m = (localKey.height - localRect.top - localRect.bottom - localKey.icon
						.getIntrinsicHeight()) / 2 + localRect.top;
				localCanvas.translate(k, m);
				localKey.icon.setBounds(0, 0,
						localKey.icon.getIntrinsicWidth(),
						localKey.icon.getIntrinsicHeight());
				localKey.icon.draw(localCanvas);
				localCanvas.translate(-k, -m);
			}
		}

		return null;
	}

	private void createKeyImageViews(Keyboard.Key[] paramArrayOfKey) {
		int i = paramArrayOfKey.length;
		if (this.mKeyImageViews != null) {
			ImageView[] arrayOfImageView = this.mKeyImageViews;
			int k = arrayOfImageView.length;
			for (int m = 0; m < k; m++) {
				removeView(arrayOfImageView[m]);
			}
			this.mKeyImageViews = null;
		}
		if (this.mKeyImageViews == null) {
			this.mKeyImageViews = new ImageView[i];
		}
		for (int j = 0; j < i; j++) {
			if (this.mKeyImageViews[j] != null) {
				removeView(this.mKeyImageViews[j]);
			} else {
				this.mKeyImageViews[j] = createKeyImageView(j);
			}
		}
	}

	private void removeMessages() {
	}

	private void sendAccessibilityEventForUnicodeCharacter(int paramInt1, int paramInt2) {
		
//		AccessibilityEvent localAccessibilityEvent = null;
//		String str;
//		
//		if (this.mAccessibilityManager.isEnabled()) {
//			localAccessibilityEvent = AccessibilityEvent.obtain(paramInt1);
//			onInitializeAccessibilityEvent(localAccessibilityEvent);
//			int i = Settings.Secure.getInt(this.mContext.getContentResolver(),
//					"speak_password", 0);
//			int j = 0;
//			if (i != 0) {
//				j = 1;
//			}
//			if ((j == 0) && (!this.mAudioManager.isBluetoothA2dpOn())
//					&& (!this.mAudioManager.isWiredHeadsetOn())) {
//				break label273;
//			}
//			switch (paramInt2) {
//			default:
//				str = String.valueOf((char) paramInt2);
//			}
//		}
//		for (;;) {
//			localAccessibilityEvent.getText().add(r);
//			this.mAccessibilityManager
//					.sendAccessibilityEvent(localAccessibilityEvent);
//			return;
//			str = this.mContext.getString(2131427343);
//			continue;
//			str = this.mContext.getString(2131427344);
//			continue;
//			str = this.mContext.getString(2131427345);
//			continue;
//			str = this.mContext.getString(2131427346);
//			continue;
//			str = this.mContext.getString(2131427347);
//			continue;
//			str = this.mContext.getString(2131427348);
//			continue;
//			str = this.mContext.getString(2131427349);
//			continue;
//			label273: if (!this.mHeadsetRequiredToHearPasswordsAnnounced) {
//				if (paramInt1 == 256) {
//					this.mHeadsetRequiredToHearPasswordsAnnounced = true;
//				}
//				str = this.mContext.getString(2131427350);
//			} else {
//				str = this.mContext.getString(2131427351);
//			}
//		}
	}

	public int getColCount() {
		return this.mColCount;
	}

	public Keyboard.Key getFocusedKey() {
		if (this.mFocusIndex == -1) {
			return null;
		}
		return this.mKeys[this.mFocusIndex];
	}

	public Keyboard.Key getKey(int paramInt) {
		if ((this.mKeys == null) || (this.mKeys.length == 0) || (paramInt < 0)
				|| (paramInt > this.mKeys.length)) {
			return null;
		}
		return this.mKeys[paramInt];
	}

	public Keyboard getKeyboard() {
		return this.mKeyboard;
	}

	public int getNearestIndex(float x, float y) {
		if ((this.mKeys == null) || (this.mKeys.length == 0)) {
			return 0;
		}
		float adjustX = x - getPaddingLeft();
		float adjustY = y - getPaddingTop();
		float height = getMeasuredHeight() - getPaddingTop()
				- getPaddingBottom();
		float width = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
		int rowCount = getRowCount();
		int columnCount = getColCount();

		int rowIndex = (int) (adjustY / height * rowCount);
		int columnIndex = (int) (adjustX / width * columnCount);

		int index = columnIndex + rowIndex * columnCount;

		if ((index > 46) && (index < 53)) {
			return 46;
		} else if (index >= 53) {
			return index - 6;
		} else if (index >= 0) {
			return index;
		} else {
			return 0;
		}
	}

	public int getRowCount() {
		return this.mRowCount;
	}

	public int getShiftState() {
		return this.mShiftState;
	}

	public void invalidateAllKeys() {
		createKeyImageViews(this.mKeys);
	}

	public void invalidateKey(int paramInt) {
		if (this.mKeys == null) {
			return;
		}
		if ((paramInt < 0) || (paramInt >= this.mKeys.length)) {
			return;
		}

		if (this.mKeyImageViews[paramInt] != null) {
			removeView(this.mKeyImageViews[paramInt]);
		}
		this.mKeyImageViews[paramInt] = createKeyImageView(paramInt);
	}

	public boolean isMiniKeyboardOnScreen() {
		return this.mMiniKeyboardOnScreen;
	}

	public boolean isShifted() {
		return (this.mShiftState == 1) || (this.mShiftState == 2);
	}

	public void onDismissMiniKeyboard() {
		this.mMiniKeyboardOnScreen = false;
		List<Key> localList = this.mKeyboard.getKeys();
		this.mKeys = ((Keyboard.Key[]) localList
				.toArray(new Keyboard.Key[localList.size()]));
		invalidateAllKeys();
	}

	public void onDraw(Canvas paramCanvas) {
		super.onDraw(paramCanvas);
	}

	public void onKeyLongPress() {
		int resId = this.mKeys[this.mFocusIndex].popupResId;

		if (resId != 0) {
			this.mMiniKeyboardOnScreen = true;
			List<Key> localList = new Keyboard(this.mContext, resId).getKeys();
			int j = localList.size();
			int k = this.mFocusIndex;
			int m = this.mFocusIndex / this.mColCount;
			int n = (j + this.mFocusIndex) / this.mColCount;
			if (m != n) {
				k = n * this.mColCount - j;
			}

			for (int i = 0; i < j; i++) {
				Keyboard.Key localKey = (Keyboard.Key) localList.get(i);
				localKey.x = this.mKeys[(k + i)].x;
				localKey.y = this.mKeys[(k + i)].y;
				this.mKeys[(k + i)] = localKey;
				localKey.sticky = true;
			}
			invalidateAllKeys();
		}
	}

	public void onMeasure(int paramInt1, int paramInt2) {
		super.onMeasure(paramInt1, paramInt2);
		if (this.mKeyboard == null) {
			setMeasuredDimension(this.mPaddingLeft + this.mPaddingRight,
					this.mPaddingTop + this.mPaddingBottom);
			return;
		}
		int i = this.mKeyboard.getMinWidth() + this.mPaddingLeft
				+ this.mPaddingRight;
		if (View.MeasureSpec.getSize(paramInt1) < i + 10) {
			i = View.MeasureSpec.getSize(paramInt1);
		}
		setMeasuredDimension(i, this.mKeyboard.getHeight() + this.mPaddingTop
				+ this.mPaddingBottom);
	}

	public void setFocus(int paramInt1, int paramInt2, boolean paramBoolean) {
		setFocus(paramInt2 + paramInt1 * this.mColCount, paramBoolean);
	}

	public void setFocus(int paramInt, boolean paramBoolean) {
		setFocus(paramInt, paramBoolean, true);
	}

	public void setFocus(int paramInt, boolean paramBoolean1,
			boolean paramBoolean2) {
		float f = 1.0F;
		if ((this.mKeyImageViews == null) || (this.mKeyImageViews.length == 0)) {
			return;
		}

		do {
			if ((paramInt < 0) || (paramInt >= this.mKeyImageViews.length)) {
				paramInt = -1;
			}
		} while ((paramInt == this.mFocusIndex)
				&& (paramBoolean1 == this.mFocusClicked));

		if (this.mFocusIndex != -1) {
			int j = this.mKeys[this.mFocusIndex].codes[0];
			sendAccessibilityEventForUnicodeCharacter(256, j);
			sendAccessibilityEventForUnicodeCharacter(65536, j);
		}

		if (paramInt != -1) {
			int i = this.mKeys[paramInt].codes[0];
			sendAccessibilityEventForUnicodeCharacter(128, i);
			sendAccessibilityEventForUnicodeCharacter(32768, i);
		}

		if (this.mCurrentFocusView != null) {
			this.mCurrentFocusView.animate().scaleX(f).scaleY(f)
					.setStartDelay(this.mUnfocusStartDelay);
			this.mCurrentFocusView.animate().setDuration(this.mClickAnimDur)
					.setStartDelay(this.mUnfocusStartDelay);
		}

		if (paramInt != -1) {
			if (!paramBoolean1) {
				if (paramBoolean2) {
					f = this.mFocusedScale;
				}
			}
			f = this.mClickedScale;
		}
		this.mCurrentFocusView = this.mKeyImageViews[paramInt];
		this.mCurrentFocusView.animate().scaleX(f).scaleY(f);
		this.mCurrentFocusView.animate().setDuration(this.mClickAnimDur)
				.start();
		this.mFocusIndex = paramInt;
		this.mFocusClicked = paramBoolean1;
	}

	public void setKeyboard(Keyboard paramKeyboard) {
		removeMessages();
		this.mKeyboard = paramKeyboard;
		List<Key> localList = this.mKeyboard.getKeys();
		this.mKeys = ((Keyboard.Key[]) localList
				.toArray(new Keyboard.Key[localList.size()]));
		int i = this.mShiftState;
		this.mShiftState = -1;
		setShiftState(i);
		requestLayout();
		invalidateAllKeys();
	}

	public void setShiftState(int paramInt) {
		if (this.mShiftState == paramInt) {
			return;
		}
		this.mShiftState = paramInt;
		if (paramInt == 0) {
			this.mKeyboard.setShifted(false);
		} else {
			this.mKeyboard.setShifted(true);
		}
		invalidateAllKeys();
	}
}
