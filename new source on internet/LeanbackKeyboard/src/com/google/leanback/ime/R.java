package com.google.leanback.ime;

public final class R
{
  public static final class array
  {
    public static final int movement_sensitivity_values = 2131492864;
  }
  
  public static final class attr
  {
    public static final int columnCount = 2130771969;
    public static final int disabledBackgroundColor = 2130771971;
    public static final int enabledBackgroundColor = 2130771970;
    public static final int headerAlpha = 2130771977;
    public static final int levelsCenterX = 2130771975;
    public static final int levelsCenterY = 2130771976;
    public static final int minLevelRadius = 2130771974;
    public static final int primaryLevels = 2130771972;
    public static final int rowCount = 2130771968;
    public static final int soundPrimaryLevel = 2130771979;
    public static final int soundTrailLevel = 2130771978;
    public static final int trailLevels = 2130771973;
  }
  
  public static final class bool
  {
    public static final int im_is_default = 2131099648;
  }
  
  public static final class color
  {
    public static final int candidate_background = 2131165190;
    public static final int candidate_font_color = 2131165191;
    public static final int enter_key_font_color = 2131165193;
    public static final int ime_selector_color = 2131165189;
    public static final int ime_selector_focus_color = 2131165188;
    public static final int key_text_default = 2131165187;
    public static final int keyboard_background = 2131165186;
    public static final int search_mic_background = 2131165185;
    public static final int search_mic_levels_guideline = 2131165184;
    public static final int separator_color = 2131165192;
  }
  
  public static final class dimen
  {
    public static final int action_button_margin_left = 2131296270;
    public static final int action_button_margin_right = 2131296271;
    public static final int action_button_margin_top = 2131296272;
    public static final int action_button_size = 2131296273;
    public static final int cancel_region_margin_left = 2131296262;
    public static final int candidate_divider_spacing = 2131296282;
    public static final int candidate_font_size = 2131296280;
    public static final int candidate_padding_horizontal = 2131296281;
    public static final int enter_key_font_size = 2131296278;
    public static final int function_key_font_size = 2131296286;
    public static final int function_key_mode_change_font_size = 2131296287;
    public static final int function_keyboard_margin = 2131296283;
    public static final int grid_margin_bottom = 2131296277;
    public static final int grid_margin_top = 2131296276;
    public static final int key_circle_focused_padding = 2131296269;
    public static final int key_circle_height = 2131296267;
    public static final int key_circle_width = 2131296268;
    public static final int key_font_size = 2131296285;
    public static final int key_height = 2131296256;
    public static final int key_width = 2131296257;
    public static final int keyboard_container_height = 2131296284;
    public static final int keyboard_horizontal_gap = 2131296259;
    public static final int keyboard_horz_spacing = 2131296266;
    public static final int keyboard_top_spacing = 2131296264;
    public static final int keyboard_vert_spacing = 2131296265;
    public static final int keyboard_vertical_gap = 2131296260;
    public static final int keyboard_width = 2131296274;
    public static final int mode_change_key_font_size = 2131296279;
    public static final int recognizer_size = 2131296261;
    public static final int resize_move_distance = 2131296290;
    public static final int selector_size = 2131296263;
    public static final int space_key_width = 2131296258;
    public static final int textbox_height = 2131296275;
    public static final int touch_indicator_android_size = 2131296289;
    public static final int touch_indicator_size = 2131296288;
  }
  
  public static final class drawable
  {
    public static final int ic_ime_accent_close = 2130837504;
    public static final int ic_ime_delete = 2130837505;
    public static final int ic_ime_focus = 2130837506;
    public static final int ic_ime_focus_rect_normal = 2130837507;
    public static final int ic_ime_focus_rect_pressed = 2130837508;
    public static final int ic_ime_left_arrow = 2130837509;
    public static final int ic_ime_return = 2130837510;
    public static final int ic_ime_right_arrow = 2130837511;
    public static final int ic_ime_selector = 2130837512;
    public static final int ic_ime_shift_lock_on = 2130837513;
    public static final int ic_ime_shift_off = 2130837514;
    public static final int ic_ime_shift_on = 2130837515;
    public static final int ic_ime_space = 2130837516;
    public static final int ic_ime_symbols = 2130837517;
    public static final int ic_ime_voice = 2130837518;
    public static final int ic_voice_available = 2130837519;
    public static final int ic_voice_focus = 2130837520;
    public static final int ic_voice_off = 2130837521;
    public static final int ic_voice_recording = 2130837522;
    public static final int key_circle = 2130837523;
    public static final int key_rectangle = 2130837524;
    public static final int selector_caps_shift = 2130837525;
    public static final int suggestions_divider = 2130837526;
    public static final int touch_circle = 2130837527;
    public static final int touch_rectangle = 2130837528;
    public static final int vs_micbtn_off_selector = 2130837529;
    public static final int vs_micbtn_on_selector = 2130837530;
    public static final int vs_micbtn_rec_selector = 2130837531;
    public static final int vs_reactive_dark = 2130837532;
    public static final int vs_reactive_light = 2130837533;
  }
  
  public static final class fraction
  {
    public static final int alpha_in = 2131230722;
    public static final int alpha_out = 2131230723;
    public static final int clicked_scale = 2131230720;
    public static final int focused_scale = 2131230721;
  }
  
  public static final class id
  {
    public static final int accent_bg = 2131558400;
    public static final int candidate_background = 2131558409;
    public static final int enter = 2131558405;
    public static final int key_circle = 2131558414;
    public static final int key_rectangle = 2131558413;
    public static final int key_selector = 2131558412;
    public static final int keyboard = 2131558402;
    public static final int main_keyboard = 2131558403;
    public static final int microphone = 2131558406;
    public static final int recognizer_mic_button = 2131558407;
    public static final int root_ime = 2131558408;
    public static final int suggestions = 2131558411;
    public static final int suggestions_container = 2131558410;
    public static final int text = 2131558401;
    public static final int touch_circle = 2131558417;
    public static final int touch_indicator = 2131558415;
    public static final int touch_rectangle = 2131558416;
    public static final int voice = 2131558404;
  }
  
  public static final class integer
  {
    public static final int clicked_anim_duration = 2131361792;
    public static final int unfocused_anim_delay = 2131361793;
    public static final int voice_anim_duration = 2131361794;
  }
  
  public static final class layout
  {
    public static final int accent_bg = 2130903040;
    public static final int accent_keyboard = 2130903041;
    public static final int candidate = 2130903042;
    public static final int input_leanback = 2130903043;
    public static final int recognizer_view = 2130903044;
    public static final int root_leanback = 2130903045;
    public static final int selector = 2130903046;
  }
  
  public static final class raw
  {
    public static final int domain_en = 2131034112;
  }
  
  public static final class string
  {
    public static final int abc = 2131427336;
    public static final int btn_off = 2131427341;
    public static final int btn_on = 2131427340;
    public static final int ime_name = 2131427328;
    public static final int ime_service_name = 2131427329;
    public static final int keyboard_headset_required_to_hear_password = 2131427350;
    public static final int keyboard_password_character_no_headset = 2131427351;
    public static final int keyboardview_keycode_alt = 2131427343;
    public static final int keyboardview_keycode_cancel = 2131427344;
    public static final int keyboardview_keycode_delete = 2131427345;
    public static final int keyboardview_keycode_done = 2131427346;
    public static final int keyboardview_keycode_enter = 2131427349;
    public static final int keyboardview_keycode_mode_change = 2131427347;
    public static final int keyboardview_keycode_shift = 2131427348;
    public static final int label_done_key = 2131427335;
    public static final int label_go_key = 2131427331;
    public static final int label_next_key = 2131427332;
    public static final int label_search_key = 2131427334;
    public static final int label_send_key = 2131427333;
    public static final int settings_title = 2131427338;
    public static final int subtype_generic = 2131427342;
    public static final int sym = 2131427337;
    public static final int title_movement_sensitivity = 2131427339;
    public static final int word_separators = 2131427330;
  }
  
  public static final class styleable
  {
    public static final int[] BitmapSoundLevelView = { 2130771970, 2130771971, 2130771972, 2130771973, 2130771974, 2130771975, 2130771976 };
    public static final int BitmapSoundLevelView_disabledBackgroundColor = 1;
    public static final int BitmapSoundLevelView_enabledBackgroundColor = 0;
    public static final int BitmapSoundLevelView_levelsCenterX = 5;
    public static final int BitmapSoundLevelView_levelsCenterY = 6;
    public static final int BitmapSoundLevelView_minLevelRadius = 4;
    public static final int BitmapSoundLevelView_primaryLevels = 2;
    public static final int BitmapSoundLevelView_trailLevels = 3;
    public static final int[] LeanbackKeyboardView = { 2130771968, 2130771969 };
    public static final int LeanbackKeyboardView_columnCount = 1;
    public static final int LeanbackKeyboardView_rowCount = 1;
  }
  
  public static final class xml
  {
    public static final int accent_a = 2130968576;
    public static final int accent_c = 2130968577;
    public static final int accent_e = 2130968578;
    public static final int accent_i = 2130968579;
    public static final int accent_n = 2130968580;
    public static final int accent_o = 2130968581;
    public static final int accent_s = 2130968582;
    public static final int accent_u = 2130968583;
    public static final int method = 2130968584;
    public static final int number_leanback = 2130968585;
    public static final int qwerty_leanback = 2130968586;
    public static final int sym_leanback = 2130968587;
  }
}


/* Location:              C:\Users\Administrator\Desktop\Leanback_Keyboard_L-1236599_dex2jar.jar!\com\google\leanback\ime\R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */