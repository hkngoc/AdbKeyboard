package com.google.leanback.ime;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.inputmethodservice.InputMethodService;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

@SuppressLint("NewApi")
public class LeanbackImeService extends InputMethodService {
	@SuppressWarnings("unused")
	private static final boolean DEBUG = false;
	public static final String IME_CLOSE = "com.google.android.athome.action.IME_CLOSE";
	public static final String IME_OPEN = "com.google.android.athome.action.IME_OPEN";
	public static final int MAX_SUGGESTIONS = 10;
	static final int MODE_FREE_MOVEMENT = 1;
	static final int MODE_TRACKPAD_NAVIGATION = 0;
	@SuppressWarnings("unused")
	private static final String TAG = "LbImeService";
	@SuppressWarnings("unused")
	private boolean mEnterSpaceBeforeCommitting;
	private View mInputView;
	private LeanbackKeyboardController mKeyboardController;
	private LeanbackSuggestionsFactory mSuggestionsFactory;

	private LeanbackKeyboardController.InputListener mInputListener = new LeanbackKeyboardController.InputListener() {
		public void onEntry(int paramAnonymousInt1, int paramAnonymousInt2,
				CharSequence paramAnonymousCharSequence) {
			LeanbackImeService.this.handleTextEntry(paramAnonymousInt1,
					paramAnonymousInt2, paramAnonymousCharSequence);
		}
	};

	public LeanbackImeService() {
		if (!enableHardwareAcceleration()) {
			Log.w("LbImeService", "Could not enable hardware acceleration");
		}
	}

	@SuppressWarnings("unused")
	private String getEditorText(InputConnection paramInputConnection) {
		StringBuilder localStringBuilder = new StringBuilder();
		CharSequence localCharSequence1 = paramInputConnection
				.getTextBeforeCursor(1000, 0);
		CharSequence localCharSequence2 = paramInputConnection
				.getTextAfterCursor(1000, 0);
		if (localCharSequence1 != null) {
			localStringBuilder.append(localCharSequence1);
		}
		if (localCharSequence2 != null) {
			localStringBuilder.append(localCharSequence2);
		}
		return localStringBuilder.toString();
	}

	private void handleTextEntry(int paramInt1, int paramInt2, CharSequence paramCharSequence) {
//		InputConnection localInputConnection = getCurrentInputConnection();
//		switch (paramInt1) {
//		}
//
//		for (;;) {
//			if (this.mKeyboardController.areSuggestionsEnabled()) {
//				this.mSuggestionsFactory.onKey(paramInt1, paramInt2,
//						paramCharSequence,
//						getEditorText(getCurrentInputConnection()));
//				this.mKeyboardController
//						.updateSuggestions(this.mSuggestionsFactory
//								.getSuggestions());
//			}
//			return;
//			sendDefaultEditorAction(false);
//			continue;
//			localInputConnection.deleteSurroundingText(1, 0);
//			this.mEnterSpaceBeforeCommitting = false;
//			continue;
//			CharSequence localCharSequence1 = localInputConnection.getTextBeforeCursor(1000, 0);
//			int i;
//			if (localCharSequence1 == null) {
//				i = 0;
//				label147: if (paramInt1 != 3) {
//					break label187;
//				}
//				if (i > 0) {
//					i--;
//				}
//			}
//			
//			for (;;) {
//				localInputConnection.setSelection(i, i);
//				break;
//				i = localCharSequence1.length();
//				break label147;
//				CharSequence localCharSequence2;
//				label187: localCharSequence2 = localInputConnection.getTextAfterCursor(1000, 0);
//				if ((localCharSequence2 != null) && (localCharSequence2.length() > 0)) {
//					i++;
//				}
//			}
//			
//			if ((this.mEnterSpaceBeforeCommitting)
//					&& (this.mKeyboardController.enableAutoEnterSpace())) {
//				if (LeanbackUtils.isAlphabet(paramInt2)) {
//					getCurrentInputConnection().commitText(" ", 1);
//				}
//				this.mEnterSpaceBeforeCommitting = false;
//			}
//			
//			getCurrentInputConnection().commitText(paramCharSequence, 1);
//			if (paramInt2 == 46) {
//				this.mEnterSpaceBeforeCommitting = true;
//				continue;
//				localInputConnection.deleteSurroundingText(
//						this.mSuggestionsFactory.getCursorPosition(),
//						this.mSuggestionsFactory.getComposingString().length()
//								- this.mSuggestionsFactory.getCursorPosition());
//				localInputConnection.commitText(paramCharSequence, 1);
//				this.mEnterSpaceBeforeCommitting = true;
//				continue;
//				localInputConnection.performEditorAction(1);
//				continue;
//				localInputConnection.performEditorAction(2);
//			}
//		}
	}

	public View onCreateInputView() {
		this.mInputView = this.mKeyboardController.getView();
		this.mInputView.requestFocus();
		return this.mInputView;
	}

	public void onDisplayCompletions(CompletionInfo[] paramArrayOfCompletionInfo) {
		if (this.mKeyboardController.areSuggestionsEnabled()) {
			this.mSuggestionsFactory
					.onDisplayCompletions(paramArrayOfCompletionInfo);
			this.mKeyboardController.updateSuggestions(this.mSuggestionsFactory
					.getSuggestions());
		}
	}

	public boolean onEvaluateFullscreenMode() {
		return false;
	}

	public boolean onEvaluateInputViewShown() {
		return true;
	}

	public void onFinishInputView(boolean paramBoolean) {
		super.onFinishInputView(paramBoolean);
		sendBroadcast(new Intent("com.google.android.athome.action.IME_CLOSE"));
	}

	@SuppressLint("NewApi")
	public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
		if ((isInputViewShown())
				&& ((0x200000 & paramMotionEvent.getSource()) == 2097152)
				&& (this.mKeyboardController
						.onGenericMotionEvent(paramMotionEvent))) {
			return true;
		}
		return super.onGenericMotionEvent(paramMotionEvent);
	}

	public void onHideIme() {
		requestHideSelf(0);
	}

	public void onInitializeInterface() {
		this.mKeyboardController = new LeanbackKeyboardController(this,
				this.mInputListener);
		this.mEnterSpaceBeforeCommitting = false;
		this.mSuggestionsFactory = new LeanbackSuggestionsFactory(this, 10);
	}

	public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
		if ((isInputViewShown())
				&& (this.mKeyboardController.onKeyDown(paramInt, paramKeyEvent))) {
			return true;
		}
		return super.onKeyDown(paramInt, paramKeyEvent);
	}

	public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
		if ((isInputViewShown())
				&& (this.mKeyboardController.onKeyUp(paramInt, paramKeyEvent))) {
			return true;
		}
		return super.onKeyUp(paramInt, paramKeyEvent);
	}

	public boolean onShowInputRequested(int paramInt, boolean paramBoolean) {
		return true;
	}

	public void onStartInput(EditorInfo paramEditorInfo, boolean paramBoolean) {
		super.onStartInput(paramEditorInfo, paramBoolean);
		this.mEnterSpaceBeforeCommitting = false;
		this.mSuggestionsFactory.onStartInput(paramEditorInfo);
		this.mKeyboardController.onStartInput(paramEditorInfo);
	}

	public void onStartInputView(EditorInfo paramEditorInfo,
			boolean paramBoolean) {
		super.onStartInputView(paramEditorInfo, paramBoolean);
		this.mKeyboardController.onStartInputView();
		sendBroadcast(new Intent("com.google.android.athome.action.IME_OPEN"));
	}
}
