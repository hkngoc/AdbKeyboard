package com.google.leanback.ime;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PointF;
import android.graphics.Rect;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.google.leanback.ime.voice.RecognizerView;
import com.google.leanback.ime.voice.RecognizerView.Callback;
import com.google.leanback.ime.voice.SpeechLevelSource;
import java.util.ArrayList;

public class LeanbackKeyboardContainer {
	private static final boolean DEBUG = false;
	public static final double DIRECTION_STEP_MULTIPLIER = 1.25D;
	private static final String IME_PRIVATE_OPTIONS_ESCAPE_NORTH = "EscapeNorth=1";
	private static final String IME_PRIVATE_OPTIONS_VOICE_DISMISS = "VoiceDismiss=1";
	private static final long MOVEMENT_ANIMATION_DURATION = 150L;
	private static final int MSG_START_INPUT_VIEW = 0;
	protected static final float PHYSICAL_HEIGHT_CM = 5.0F;
	protected static final float PHYSICAL_WIDTH_CM = 12.0F;
	private static final String TAG = "LbKbContainer";
	public static final double TOUCH_MOVE_MIN_DISTANCE = 0.1D;
	public static final int TOUCH_STATE_CLICK = 3;
	public static final int TOUCH_STATE_NO_TOUCH = 0;
	public static final int TOUCH_STATE_TOUCH_MOVE = 2;
	public static final int TOUCH_STATE_TOUCH_SNAP = 1;
	private static final boolean VOICE_SUPPORTED = true;
	private static final Interpolator sMovementInterpolator = new DecelerateInterpolator(
			1.5F);
	private Button mActionButtonView;
	private final float mAlphaIn;
	private final float mAlphaOut;
	private boolean mAutoEnterSpaceEnabled;
	private boolean mCapCharacters;
	private boolean mCapSentences;
	private boolean mCapWords;
	private final int mClickAnimDur;
	private LeanbackImeService mContext;
	private KeyFocus mCurrKeyInfo = new KeyFocus();
	private DismissListener mDismissListener;
	private KeyFocus mDownKeyInfo = new KeyFocus();
	private int mEnterKeyTextResId;
	private boolean mEscapeNorthEnabled;
	private Keyboard mInitialMainKeyboard;
	private View mKeySelector;
	private View mKeyboardsContainer;
	private LeanbackKeyboardView mMainKeyboardView;
	private Keyboard mNumKeyboard;
	private PointF mPhysicalSelectPos = new PointF(2.0F, 0.5F);
	private PointF mPhysicalTouchPos = new PointF(2.0F, 0.5F);
	private LeanbackKeyboardView mPrevView;
	private Keyboard mQwertyKeyboard;
	private Intent mRecognizerIntent;
	private Rect mRect = new Rect();
	private RelativeLayout mRootView;
	private View mSelector;
	private SpeechLevelSource mSpeechLevelSource;
	private SpeechRecognizer mSpeechRecognizer;
	private LinearLayout mSuggestions;
	private HorizontalScrollView mSuggestionsContainer;
	private boolean mSuggestionsEnabled;
	private Keyboard mSymKeyboard;
	private KeyFocus mTempKeyInfo = new KeyFocus();
	private PointF mTempPoint = new PointF();
	private boolean mTouchDown = false;
	private View mTouchIndicator;
	private ValueAnimator mTouchIndicatorAnimator;
	private int mTouchState = 0;
	private final int mVoiceAnimDur;
	private final VoiceIntroAnimator mVoiceAnimator;
	private RecognizerView mVoiceButtonView;
	private boolean mVoiceEnabled;
	
	private Animator.AnimatorListener mVoiceEnterListener = new Animator.AnimatorListener() {
		public void onAnimationCancel(Animator paramAnonymousAnimator) {
		}

		public void onAnimationEnd(Animator paramAnonymousAnimator) {
		}

		public void onAnimationRepeat(Animator paramAnonymousAnimator) {
		}

		public void onAnimationStart(Animator paramAnonymousAnimator) {
			LeanbackKeyboardContainer.this.mSelector.setVisibility(4);
			LeanbackKeyboardContainer.this
					.startRecognition(LeanbackKeyboardContainer.this.mContext);
		}
	};
	
	private Animator.AnimatorListener mVoiceExitListener = new Animator.AnimatorListener() {
		public void onAnimationCancel(Animator paramAnonymousAnimator) {
		}

		public void onAnimationEnd(Animator paramAnonymousAnimator) {
			LeanbackKeyboardContainer.this.mSelector.setVisibility(0);
		}

		public void onAnimationRepeat(Animator paramAnonymousAnimator) {
		}

		public void onAnimationStart(Animator paramAnonymousAnimator) {
			LeanbackKeyboardContainer.this.mVoiceButtonView.showNotListening();
			LeanbackKeyboardContainer.this.mSpeechRecognizer.cancel();
			LeanbackKeyboardContainer.this.mSpeechRecognizer
					.setRecognitionListener(null);
			LeanbackKeyboardContainer.access$1102(
					LeanbackKeyboardContainer.this, false);
		}
	};
	
	private boolean mVoiceKeyDismissesEnabled;
	private VoiceListener mVoiceListener;
	private boolean mVoiceOn;
	private float mX;
	private float mY;

	public LeanbackKeyboardContainer(Context paramContext) {
		this.mContext = ((LeanbackImeService) paramContext);
		Resources localResources = this.mContext.getResources();
		this.mVoiceAnimDur = localResources.getInteger(2131361794);
		this.mAlphaIn = localResources.getFraction(2131230722, 1, 1);
		this.mAlphaOut = localResources.getFraction(2131230723, 1, 1);
		this.mVoiceAnimator = new VoiceIntroAnimator(this.mVoiceEnterListener,
				this.mVoiceExitListener);
		initKeyboards();
		this.mRootView = ((RelativeLayout) this.mContext.getLayoutInflater()
				.inflate(2130903045, null));
		this.mKeyboardsContainer = this.mRootView.findViewById(2131558402);
		this.mSuggestionsContainer = ((HorizontalScrollView) this.mRootView
				.findViewById(2131558410));
		this.mSuggestions = ((LinearLayout) this.mSuggestionsContainer
				.findViewById(2131558411));
		this.mMainKeyboardView = ((LeanbackKeyboardView) this.mRootView
				.findViewById(2131558403));
		this.mVoiceButtonView = ((RecognizerView) this.mRootView
				.findViewById(2131558404));
		this.mActionButtonView = ((Button) this.mRootView
				.findViewById(2131558405));
		this.mSelector = this.mContext.getLayoutInflater().inflate(2130903046,
				this.mRootView, false);
		this.mRootView.addView(this.mSelector);
		this.mKeySelector = this.mSelector.findViewById(2131558412);
		this.mTouchIndicator = this.mSelector.findViewById(2131558415);
		float f = paramContext.getResources().getFraction(2131230720, 1, 1);
		this.mClickAnimDur = paramContext.getResources().getInteger(2131361792);
		this.mTouchIndicatorAnimator = ValueAnimator.ofFloat(new float[] {
				1.0F, f });
		this.mTouchIndicatorAnimator.setDuration(this.mClickAnimDur);
		this.mTouchIndicatorAnimator
				.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
					public void onAnimationUpdate(
							ValueAnimator paramAnonymousValueAnimator) {
						float f = ((Float) paramAnonymousValueAnimator
								.getAnimatedValue()).floatValue();
						LeanbackKeyboardContainer.this.mTouchIndicator
								.setScaleX(f);
						LeanbackKeyboardContainer.this.mTouchIndicator
								.setScaleY(f);
					}
				});
		this.mSpeechLevelSource = new SpeechLevelSource();
		this.mVoiceButtonView.setSpeechLevelSource(this.mSpeechLevelSource);
		this.mSpeechRecognizer = SpeechRecognizer
				.createSpeechRecognizer(this.mContext);
		this.mVoiceButtonView.setCallback(new RecognizerView.Callback() {
			public void onCancelRecordingClicked() {
				LeanbackKeyboardContainer.this.cancelVoiceRecording();
			}

			public void onStartRecordingClicked() {
				LeanbackKeyboardContainer.this.startVoiceRecording();
			}

			public void onStopRecordingClicked() {
				LeanbackKeyboardContainer.this.cancelVoiceRecording();
			}
		});
	}

	private void configureFocus(KeyFocus paramKeyFocus, Rect paramRect,
			int paramInt1, int paramInt2) {
		paramKeyFocus.type = paramInt2;
		paramKeyFocus.index = paramInt1;
		paramKeyFocus.rect.set(paramRect);
	}

	private void configureFocus(KeyFocus paramKeyFocus, Rect paramRect,
			int paramInt1, Keyboard.Key paramKey, int paramInt2) {
		paramKeyFocus.type = paramInt2;
		if (paramKey == null) {
			return;
		}
		if (paramKey.codes != null) {
		}
		for (paramKeyFocus.code = paramKey.codes[0];; paramKeyFocus.code = 0) {
			paramKeyFocus.index = paramInt1;
			paramKeyFocus.label = paramKey.label;
			paramKeyFocus.rect.left = (paramKey.x + paramRect.left);
			paramKeyFocus.rect.top = (paramKey.y + paramRect.top);
			paramKeyFocus.rect.right = (paramKeyFocus.rect.left + paramKey.width);
			paramKeyFocus.rect.bottom = (paramKeyFocus.rect.top + paramKey.height);
			return;
		}
	}

	private void escapeNorth() {
		this.mDismissListener.onDismiss(false);
	}

	private PointF getAlignmentPosition(float paramFloat1, float paramFloat2,
			PointF paramPointF) {
		float f1 = this.mRootView.getWidth() - this.mRootView.getPaddingRight()
				- this.mRootView.getPaddingLeft()
				- this.mContext.getResources().getDimension(2131296263);
		float f2 = this.mRootView.getHeight() - this.mRootView.getPaddingTop()
				- this.mRootView.getPaddingBottom()
				- this.mContext.getResources().getDimension(2131296263);
		paramPointF.x = (f1 * (paramFloat1 / 12.0F) + this.mRootView
				.getPaddingLeft());
		paramPointF.y = (f2 * (paramFloat2 / 5.0F) + this.mRootView
				.getPaddingTop());
		return paramPointF;
	}

	private void getPhysicalPosition(float paramFloat1, float paramFloat2,
			PointF paramPointF) {
		float f1 = paramFloat1 - this.mSelector.getWidth() / 2;
		float f2 = paramFloat2 - this.mSelector.getHeight() / 2;
		float f3 = this.mRootView.getWidth() - this.mRootView.getPaddingRight()
				- this.mRootView.getPaddingLeft()
				- this.mContext.getResources().getDimension(2131296263);
		float f4 = this.mRootView.getHeight() - this.mRootView.getPaddingTop()
				- this.mRootView.getPaddingBottom()
				- this.mContext.getResources().getDimension(2131296263);
		float f5 = 12.0F * (f1 - this.mRootView.getPaddingLeft()) / f3;
		float f6 = 5.0F * (f2 - this.mRootView.getPaddingTop()) / f4;
		paramPointF.x = f5;
		paramPointF.y = f6;
	}

	private PointF getTouchSnapPosition() {
		PointF localPointF = new PointF();
		getPhysicalPosition(this.mCurrKeyInfo.rect.centerX(),
				this.mCurrKeyInfo.rect.centerY(), localPointF);
		return localPointF;
	}

	private void initKeyboards() {
		this.mQwertyKeyboard = new Keyboard(this.mContext, 2130968586);
		this.mSymKeyboard = new Keyboard(this.mContext, 2130968587);
		this.mNumKeyboard = new Keyboard(this.mContext, 2130968585);
	}

	private void offsetRect(Rect paramRect, View paramView) {
		paramRect.left = 0;
		paramRect.top = 0;
		paramRect.right = paramView.getWidth();
		paramRect.bottom = paramView.getHeight();
		this.mRootView.offsetDescendantRectToMyCoords(paramView, paramRect);
	}

	private void onToggleCapsLock() {
		onShiftDoubleClick(isCapsLockOn());
	}

	private void setImeOptions(Resources paramResources, EditorInfo paramEditorInfo)
  {
    this.mSuggestionsEnabled = true;
    this.mAutoEnterSpaceEnabled = true;
    this.mVoiceEnabled = true;
    this.mInitialMainKeyboard = this.mQwertyKeyboard;
    this.mEscapeNorthEnabled = false;
    this.mVoiceKeyDismissesEnabled = false;
    label68:
    boolean bool5;
    label89:
    boolean bool4;
    label119:
    boolean bool1;
    switch (LeanbackUtils.getInputTypeClass(paramEditorInfo))
    {
    default: 
      if (this.mSuggestionsEnabled)
      {
        if ((0x80000 & paramEditorInfo.inputType) == 0)
        {
          bool5 = true;
          this.mSuggestionsEnabled = bool5;
        }
      }
      else
      {
        if (this.mAutoEnterSpaceEnabled)
        {
          if ((!this.mSuggestionsEnabled) || (!this.mAutoEnterSpaceEnabled)) {
            break label416;
          }
          bool4 = true;
          this.mAutoEnterSpaceEnabled = bool4;
        }
        if ((0x4000 & paramEditorInfo.inputType) == 0) {
          break label422;
        }
        bool1 = true;
        label138:
        this.mCapSentences = bool1;
        if (((0x2000 & paramEditorInfo.inputType) == 0) && (LeanbackUtils.getInputTypeVariation(paramEditorInfo) != 96)) {
          break label427;
        }
      }
      break;
    }
    label416:
    label422:
    label427:
    for (boolean bool2 = true;; bool2 = false)
    {
      this.mCapWords = bool2;
      int i = 0x1000 & paramEditorInfo.inputType;
      boolean bool3 = false;
      if (i != 0) {
        bool3 = true;
      }
      this.mCapCharacters = bool3;
      if (paramEditorInfo.privateImeOptions != null)
      {
        if (paramEditorInfo.privateImeOptions.contains("EscapeNorth=1")) {
          this.mEscapeNorthEnabled = true;
        }
        if (paramEditorInfo.privateImeOptions.contains("VoiceDismiss=1")) {
          this.mVoiceKeyDismissesEnabled = true;
        }
      }
      switch (LeanbackUtils.getImeAction(paramEditorInfo))
      {
      default: 
        this.mEnterKeyTextResId = 2131427335;
        return;
        this.mSuggestionsEnabled = false;
        this.mVoiceEnabled = false;
        this.mInitialMainKeyboard = this.mQwertyKeyboard;
        break label68;
        switch (LeanbackUtils.getInputTypeVariation(paramEditorInfo))
        {
        default: 
          break;
        case 32: 
          this.mSuggestionsEnabled = true;
          this.mAutoEnterSpaceEnabled = false;
          this.mVoiceEnabled = false;
          this.mInitialMainKeyboard = this.mQwertyKeyboard;
          break;
        case 96: 
        case 128: 
        case 144: 
        case 224: 
          this.mSuggestionsEnabled = false;
          this.mVoiceEnabled = false;
          this.mInitialMainKeyboard = this.mQwertyKeyboard;
          break;
          bool5 = false;
          break label89;
          bool4 = false;
          break label119;
          bool1 = false;
          break label138;
        }
        break;
      }
    }
    this.mEnterKeyTextResId = 2131427331;
    return;
    this.mEnterKeyTextResId = 2131427332;
    return;
    this.mEnterKeyTextResId = 2131427334;
    return;
    this.mEnterKeyTextResId = 2131427333;
  }

	private void setKbFocus(KeyFocus paramKeyFocus, boolean paramBoolean)
  {
    boolean bool1 = true;
    if ((paramKeyFocus.equals(this.mCurrKeyInfo)) && (!paramBoolean)) {
      return;
    }
    LeanbackKeyboardView localLeanbackKeyboardView1 = this.mPrevView;
    this.mPrevView = null;
    int i = paramKeyFocus.type;
    boolean bool2 = false;
    switch (i)
    {
    case 2: 
    case 3: 
    default: 
      if ((localLeanbackKeyboardView1 != null) && (localLeanbackKeyboardView1 != this.mPrevView)) {
        if (this.mTouchState != 3) {
          break label215;
        }
      }
      break;
    }
    for (;;)
    {
      localLeanbackKeyboardView1.setFocus(-1, bool1);
      if (0 == 0) {
        this.mActionButtonView.setSelected(false);
      }
      resizeSelectorToFocus(paramKeyFocus.rect, bool2);
      this.mCurrKeyInfo.set(paramKeyFocus);
      return;
      this.mVoiceButtonView.setMicFocused(bool1);
      bool2 = false;
      break;
      label158:
      LeanbackKeyboardView localLeanbackKeyboardView2;
      int j;
      if (paramKeyFocus.code != 32)
      {
        bool2 = bool1;
        localLeanbackKeyboardView2 = this.mMainKeyboardView;
        j = paramKeyFocus.index;
        if (this.mTouchState != 3) {
          break label209;
        }
      }
      label209:
      for (boolean bool3 = bool1;; bool3 = false)
      {
        localLeanbackKeyboardView2.setFocus(j, bool3, bool2);
        this.mPrevView = this.mMainKeyboardView;
        break;
        bool2 = false;
        break label158;
      }
      label215:
      bool1 = false;
    }
  }

	private void setSelectorType(boolean paramBoolean) {
		int i;
		if (paramBoolean) {
			i = 0;
			this.mKeySelector.findViewById(2131558414).setVisibility(i);
			this.mTouchIndicator.findViewById(2131558417).setVisibility(i);
			if (!paramBoolean) {
				break label76;
			}
		}
		label76: for (int j = 8;; j = 0) {
			this.mKeySelector.findViewById(2131558413).setVisibility(j);
			this.mTouchIndicator.findViewById(2131558416).setVisibility(j);
			return;
			i = 8;
			break;
		}
	}

	private void setShiftState(int paramInt) {
		this.mMainKeyboardView.setShiftState(paramInt);
	}

	private void setTouchStateInternal(int paramInt) {
		this.mTouchState = paramInt;
	}

	private void startRecognition(Context paramContext) {
		this.mRecognizerIntent = new Intent(
				"android.speech.action.RECOGNIZE_SPEECH");
		this.mRecognizerIntent.putExtra("android.speech.extra.LANGUAGE_MODEL",
				"free_form");
		this.mRecognizerIntent.putExtra("android.speech.extra.PARTIAL_RESULTS",
				true);
		this.mSpeechRecognizer
				.setRecognitionListener(new RecognitionListener() {
					float peakRmsLevel = 0.0F;
					int rmsCounter = 0;

					public void onBeginningOfSpeech() {
						LeanbackKeyboardContainer.this.mVoiceButtonView
								.showRecording();
					}

					public void onBufferReceived(
							byte[] paramAnonymousArrayOfByte) {
					}

					public void onEndOfSpeech() {
						LeanbackKeyboardContainer.this.mVoiceButtonView
								.showRecognizing();
						LeanbackKeyboardContainer.access$1102(
								LeanbackKeyboardContainer.this, false);
					}

					public void onError(int paramAnonymousInt) {
						LeanbackKeyboardContainer.this.cancelVoiceRecording();
						switch (paramAnonymousInt) {
						default:
							Log.d("LbKbContainer", "recognizer other error "
									+ paramAnonymousInt);
							return;
						case 7:
							Log.d("LbKbContainer", "recognizer error no match");
							return;
						case 4:
							Log.d("LbKbContainer",
									"recognizer error server error");
							return;
						case 6:
							Log.d("LbKbContainer",
									"recognizer error speech timeout");
							return;
						}
						Log.d("LbKbContainer", "recognizer error client error");
					}

					public void onEvent(int paramAnonymousInt,
							Bundle paramAnonymousBundle) {
					}

					public void onPartialResults(Bundle paramAnonymousBundle) {
					}

					public void onReadyForSpeech(Bundle paramAnonymousBundle) {
						LeanbackKeyboardContainer.this.mVoiceButtonView
								.showListening();
					}

					public void onResults(Bundle paramAnonymousBundle) {
						ArrayList localArrayList = paramAnonymousBundle
								.getStringArrayList("results_recognition");
						if ((localArrayList != null)
								&& (LeanbackKeyboardContainer.this.mVoiceListener != null)) {
							LeanbackKeyboardContainer.this.mVoiceListener
									.onVoiceResult((String) localArrayList
											.get(0));
						}
						LeanbackKeyboardContainer.this.cancelVoiceRecording();
					}

					/* Error */
					public void onRmsChanged(float paramAnonymousFloat) {
						// Byte code:
						// 0: aload_0
						// 1: monitorenter
						// 2: aload_0
						// 3: getfield 21
						// com/google/leanback/ime/LeanbackKeyboardContainer$5:this$0
						// Lcom/google/leanback/ime/LeanbackKeyboardContainer;
						// 6: iconst_1
						// 7: invokestatic 48
						// com/google/leanback/ime/LeanbackKeyboardContainer:access$1102
						// (Lcom/google/leanback/ime/LeanbackKeyboardContainer;Z)Z
						// 10: pop
						// 11: aload_0
						// 12: getfield 21
						// com/google/leanback/ime/LeanbackKeyboardContainer$5:this$0
						// Lcom/google/leanback/ime/LeanbackKeyboardContainer;
						// 15: invokestatic 126
						// com/google/leanback/ime/LeanbackKeyboardContainer:access$1300
						// (Lcom/google/leanback/ime/LeanbackKeyboardContainer;)Lcom/google/leanback/ime/voice/SpeechLevelSource;
						// 18: astore 4
						// 20: fload_1
						// 21: fconst_0
						// 22: fcmpg
						// 23: ifge +66 -> 89
						// 26: iconst_0
						// 27: istore 5
						// 29: aload 4
						// 31: iload 5
						// 33: invokevirtual 131
						// com/google/leanback/ime/voice/SpeechLevelSource:setSpeechLevel
						// (I)V
						// 36: aload_0
						// 37: fload_1
						// 38: aload_0
						// 39: getfield 26
						// com/google/leanback/ime/LeanbackKeyboardContainer$5:peakRmsLevel
						// F
						// 42: invokestatic 137 java/lang/Math:max (FF)F
						// 45: putfield 26
						// com/google/leanback/ime/LeanbackKeyboardContainer$5:peakRmsLevel
						// F
						// 48: aload_0
						// 49: iconst_1
						// 50: aload_0
						// 51: getfield 28
						// com/google/leanback/ime/LeanbackKeyboardContainer$5:rmsCounter
						// I
						// 54: iadd
						// 55: putfield 28
						// com/google/leanback/ime/LeanbackKeyboardContainer$5:rmsCounter
						// I
						// 58: aload_0
						// 59: getfield 28
						// com/google/leanback/ime/LeanbackKeyboardContainer$5:rmsCounter
						// I
						// 62: bipush 100
						// 64: if_icmple +22 -> 86
						// 67: aload_0
						// 68: getfield 26
						// com/google/leanback/ime/LeanbackKeyboardContainer$5:peakRmsLevel
						// F
						// 71: fconst_0
						// 72: fcmpl
						// 73: ifne +13 -> 86
						// 76: aload_0
						// 77: getfield 21
						// com/google/leanback/ime/LeanbackKeyboardContainer$5:this$0
						// Lcom/google/leanback/ime/LeanbackKeyboardContainer;
						// 80: invokestatic 33
						// com/google/leanback/ime/LeanbackKeyboardContainer:access$600
						// (Lcom/google/leanback/ime/LeanbackKeyboardContainer;)Lcom/google/leanback/ime/voice/RecognizerView;
						// 83: invokevirtual 140
						// com/google/leanback/ime/voice/RecognizerView:showNotListening
						// ()V
						// 86: aload_0
						// 87: monitorexit
						// 88: return
						// 89: ldc -115
						// 91: fload_1
						// 92: fmul
						// 93: f2i
						// 94: istore 5
						// 96: goto -67 -> 29
						// 99: astore_2
						// 100: aload_0
						// 101: monitorexit
						// 102: aload_2
						// 103: athrow
						// Local variable table:
						// start length slot name signature
						// 0 104 0 this 5
						// 0 104 1 paramAnonymousFloat float
						// 99 4 2 localObject Object
						// 18 12 4 localSpeechLevelSource SpeechLevelSource
						// 27 68 5 i int
						// Exception table:
						// from to target type
						// 2 20 99 finally
						// 29 86 99 finally
					}
				});
		this.mSpeechRecognizer.startListening(this.mRecognizerIntent);
	}

	public void alignSelector(float paramFloat1, float paramFloat2,
			boolean paramBoolean) {
		float f1 = paramFloat1 - this.mSelector.getWidth() / 2;
		float f2 = paramFloat2 - this.mSelector.getHeight() / 2;
		if (!paramBoolean) {
			this.mSelector.setX(f1);
			this.mSelector.setY(f2);
			return;
		}
		this.mSelector.animate().x(f1).y(f2)
				.setInterpolator(sMovementInterpolator).setDuration(150L)
				.start();
	}

	public void alignSelectorToFocus(boolean paramBoolean) {
		alignSelector(this.mCurrKeyInfo.rect.centerX(),
				this.mCurrKeyInfo.rect.centerY(), paramBoolean);
	}

	public boolean areSuggestionsEnabled() {
		return this.mSuggestionsEnabled;
	}

	public void cancelVoiceRecording() {
		this.mVoiceAnimator.startExitAnimation();
	}

	public void clearSuggestions() {
		this.mSuggestions.removeAllViews();
		if (getCurrFocus().type == 3) {
			resetFocusCursor(true);
		}
	}

	public boolean enableAutoEnterSpace() {
		return this.mAutoEnterSpaceEnabled;
	}

	public void getBestFocus(float paramFloat1, float paramFloat2,
			KeyFocus paramKeyFocus) {
		offsetRect(this.mRect, this.mActionButtonView);
		int i = this.mRect.left;
		offsetRect(this.mRect, this.mMainKeyboardView);
		int j = this.mRect.top;
		if (paramFloat1 < 0.0F) {
			paramFloat1 = this.mX;
		}
		if (paramFloat2 < 0.0F) {
			paramFloat2 = this.mY;
		}
		int k = this.mSuggestions.getChildCount();
		if ((paramFloat2 < j) && (k > 0) && (this.mSuggestionsEnabled)) {
			for (int n = 0;; n++) {
				if (n < k) {
					View localView = this.mSuggestions.getChildAt(n);
					offsetRect(this.mRect, localView);
					if ((paramFloat1 < this.mRect.right) || (n + 1 == k)) {
						localView.requestFocus();
						configureFocus(paramKeyFocus, this.mRect, n, 3);
					}
				} else {
					return;
				}
			}
		}
		if ((paramFloat2 < j) && (this.mEscapeNorthEnabled)) {
			escapeNorth();
			return;
		}
		if (paramFloat1 > i) {
			offsetRect(this.mRect, this.mActionButtonView);
			configureFocus(paramKeyFocus, this.mRect, 0, 2);
			return;
		}
		this.mX = paramFloat1;
		this.mY = paramFloat2;
		offsetRect(this.mRect, this.mMainKeyboardView);
		float f1 = (int) (paramFloat1 - this.mRect.left);
		float f2 = (int) (paramFloat2 - this.mRect.top);
		int m = this.mMainKeyboardView.getNearestIndex(f1, f2);
		Keyboard.Key localKey = this.mMainKeyboardView.getKey(m);
		configureFocus(paramKeyFocus, this.mRect, m, localKey, 0);
	}

	public KeyFocus getCurrFocus() {
		return this.mCurrKeyInfo;
	}

	public int getCurrKeyCode() {
		Keyboard.Key localKey = getKey(this.mCurrKeyInfo.type,
				this.mCurrKeyInfo.index);
		int i = 0;
		if (localKey != null) {
			i = localKey.codes[0];
		}
		return i;
	}

	public Keyboard.Key getKey(int paramInt1, int paramInt2) {
		if (paramInt1 == 0) {
			return this.mMainKeyboardView.getKey(paramInt2);
		}
		return null;
	}

	public void getNextFocusInDirection(int paramInt, KeyFocus paramKeyFocus1, KeyFocus paramKeyFocus2)
  {
    switch (paramKeyFocus1.type)
    {
    case 1: 
    default: 
    case 2: 
    case 3: 
      int m;
      int n;
      label190:
      int i1;
      int i2;
      int i4;
      label268:
      int i5;
      View localView;
      do
      {
        do
        {
          do
          {
            do
            {
              return;
              offsetRect(this.mRect, this.mMainKeyboardView);
              if ((paramInt & 0x1) != 0)
              {
                getBestFocus(this.mRect.right, -1.0F, paramKeyFocus2);
                return;
              }
            } while ((paramInt & 0x8) == 0);
            offsetRect(this.mRect, this.mSuggestions);
            getBestFocus(paramKeyFocus1.rect.centerX(), this.mRect.centerY(), paramKeyFocus2);
            return;
            if ((paramInt & 0x2) != 0)
            {
              offsetRect(this.mRect, this.mMainKeyboardView);
              getBestFocus(paramKeyFocus1.rect.centerX(), this.mRect.top, paramKeyFocus2);
              return;
            }
            if ((paramInt & 0x8) == 0) {
              break;
            }
          } while (!this.mEscapeNorthEnabled);
          escapeNorth();
          return;
          if ((paramInt & 0x1) == 0) {
            break;
          }
          m = 1;
          if ((paramInt & 0x4) == 0) {
            break label368;
          }
          n = 1;
        } while ((m == 0) && (n == 0));
        offsetRect(this.mRect, this.mRootView);
        ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)this.mSuggestionsContainer.getLayoutParams();
        i1 = this.mRect.left + localMarginLayoutParams.leftMargin;
        i2 = this.mRect.right - localMarginLayoutParams.rightMargin;
        int i3 = paramKeyFocus1.index;
        if (m == 0) {
          break label374;
        }
        i4 = -1;
        i5 = i3 + i4;
        localView = this.mSuggestions.getChildAt(i5);
      } while (localView == null);
      offsetRect(this.mRect, localView);
      if ((this.mRect.left < i1) && (this.mRect.right > i2))
      {
        this.mRect.left = i1;
        this.mRect.right = i2;
      }
      for (;;)
      {
        localView.requestFocus();
        configureFocus(paramKeyFocus2, this.mRect, i5, 3);
        return;
        m = 0;
        break;
        label368:
        n = 0;
        break label190;
        label374:
        i4 = 1;
        break label268;
        if (this.mRect.left < i1)
        {
          this.mRect.right = (i1 + this.mRect.width());
          this.mRect.left = i1;
        }
        else if (this.mRect.right > i2)
        {
          this.mRect.left = (i2 - this.mRect.width());
          this.mRect.right = i2;
        }
      }
    }
    Keyboard.Key localKey = getKey(paramKeyFocus1.type, paramKeyFocus1.index);
    int i = paramKeyFocus1.rect.height() / 2;
    int j = paramKeyFocus1.rect.centerX();
    int k = paramKeyFocus1.rect.centerY();
    if (paramKeyFocus1.code == 32) {
      j = (int)this.mX;
    }
    if ((paramInt & 0x1) != 0)
    {
      if ((0x1 & localKey.edgeFlags) == 0) {
        j = paramKeyFocus1.rect.left - i;
      }
      if ((paramInt & 0x8) == 0) {
        break label656;
      }
      k = (int)(k - 1.25D * paramKeyFocus1.rect.height());
    }
    for (;;)
    {
      getPhysicalPosition(j, k, this.mTempPoint);
      getBestFocus(j, k, paramKeyFocus2);
      return;
      if ((paramInt & 0x4) == 0) {
        break;
      }
      if ((0x2 & localKey.edgeFlags) != 0)
      {
        offsetRect(this.mRect, this.mActionButtonView);
        j = this.mRect.centerX();
        break;
      }
      j = i + paramKeyFocus1.rect.right;
      break;
      label656:
      if ((paramInt & 0x2) != 0) {
        k = (int)(k + 1.25D * paramKeyFocus1.rect.height());
      }
    }
  }

	public CharSequence getSuggestionText(int paramInt) {
		CharSequence localCharSequence = null;
		if (paramInt >= 0) {
			int i = this.mSuggestions.getChildCount();
			localCharSequence = null;
			if (paramInt < i) {
				Button localButton = (Button) this.mSuggestions.getChildAt(
						paramInt).findViewById(2131558401);
				localCharSequence = null;
				if (localButton != null) {
					localCharSequence = localButton.getText();
				}
			}
		}
		return localCharSequence;
	}

	public int getTouchState() {
		return this.mTouchState;
	}

	public RelativeLayout getView() {
		return this.mRootView;
	}

	public boolean isCapsLockOn() {
		return this.mMainKeyboardView.getShiftState() == 2;
	}

	public boolean isCurrKeyShifted() {
		return this.mMainKeyboardView.isShifted();
	}

	public boolean isMiniKeyboardOnScreen() {
		return this.mMainKeyboardView.isMiniKeyboardOnScreen();
	}

	public boolean isVoiceEnabled() {
		return this.mVoiceEnabled;
	}

	public boolean isVoiceVisible() {
		return this.mVoiceButtonView.getVisibility() == 0;
	}

	public void onDismissMiniKeyboard() {
		this.mMainKeyboardView.onDismissMiniKeyboard();
	}

	public void onInitInputView() {
		resetFocusCursor(false);
		this.mSelector.setVisibility(0);
	}

	public boolean onKeyLongPress() {
		switch (this.mCurrKeyInfo.code) {
		default:
			switch (this.mCurrKeyInfo.type) {
			}
			break;
		}
		for (;;) {
			return false;
			onToggleCapsLock();
			return true;
			this.mMainKeyboardView.onKeyLongPress();
		}
	}

	public void onModeChangeClick() {
		if (this.mMainKeyboardView.getKeyboard().equals(this.mSymKeyboard)) {
			this.mMainKeyboardView.setKeyboard(this.mQwertyKeyboard);
			return;
		}
		this.mMainKeyboardView.setKeyboard(this.mSymKeyboard);
	}

	public void onPeriodEntry() {
		if (this.mMainKeyboardView.isShifted()) {
			if ((!isCapsLockOn()) && (!this.mCapCharacters)
					&& (!this.mCapWords) && (!this.mCapSentences)) {
				setShiftState(0);
			}
		}
		while ((!isCapsLockOn()) && (!this.mCapCharacters) && (!this.mCapWords)
				&& (!this.mCapSentences)) {
			return;
		}
		setShiftState(1);
	}

	public void onShiftClick() {
		if (this.mMainKeyboardView.isShifted()) {
		}
		for (int i = 0;; i = 1) {
			setShiftState(i);
			return;
		}
	}

	public void onShiftDoubleClick(boolean paramBoolean) {
		if (paramBoolean) {
		}
		for (int i = 0;; i = 2) {
			setShiftState(i);
			return;
		}
	}

	public void onSpaceEntry() {
		if (this.mMainKeyboardView.isShifted()) {
			if ((!isCapsLockOn()) && (!this.mCapCharacters)
					&& (!this.mCapWords)) {
				setShiftState(0);
			}
		}
		while ((!isCapsLockOn()) && (!this.mCapCharacters) && (!this.mCapWords)) {
			return;
		}
		setShiftState(1);
	}

	public void onStartInput(EditorInfo paramEditorInfo) {
		setImeOptions(this.mContext.getResources(), paramEditorInfo);
		this.mVoiceOn = false;
	}

	public void onStartInputView() {
		clearSuggestions();
		if (this.mSuggestionsEnabled) {
			this.mSuggestions.setVisibility(0);
		}
		for (;;) {
			this.mMainKeyboardView.setKeyboard(this.mInitialMainKeyboard);
			this.mVoiceButtonView.setMicEnabled(this.mVoiceEnabled);
			resetVoice();
			this.mActionButtonView.setText(this.mEnterKeyTextResId);
			if (!this.mCapCharacters) {
				break;
			}
			setShiftState(2);
			return;
			this.mSuggestions.setVisibility(8);
		}
		if ((this.mCapSentences) || (this.mCapWords)) {
			setShiftState(1);
			return;
		}
		setShiftState(0);
	}

	public void onTextEntry() {
		if (this.mMainKeyboardView.isShifted()) {
			if ((!isCapsLockOn()) && (!this.mCapCharacters)) {
				setShiftState(0);
			}
		}
		while ((!isCapsLockOn()) && (!this.mCapCharacters)) {
			return;
		}
		setShiftState(2);
	}

	public void onVoiceClick() {
		if (this.mVoiceButtonView != null) {
			this.mVoiceButtonView.onClick();
		}
	}

	public void resetFocusCursor(boolean paramBoolean) {
		offsetRect(this.mRect, this.mMainKeyboardView);
		this.mX = ((int) (this.mRect.left + 0.45D * this.mRect.width()));
		this.mY = ((int) (this.mRect.top + 0.375D * this.mRect.height()));
		getBestFocus(this.mX, this.mY, this.mTempKeyInfo);
		setKbFocus(this.mTempKeyInfo, true);
		alignSelectorToFocus(paramBoolean);
	}

	public void resetVoice() {
		this.mMainKeyboardView.setAlpha(this.mAlphaIn);
		this.mActionButtonView.setAlpha(this.mAlphaIn);
		this.mVoiceButtonView.setAlpha(this.mAlphaOut);
		this.mMainKeyboardView.setVisibility(0);
		this.mActionButtonView.setVisibility(0);
		this.mVoiceButtonView.setVisibility(4);
	}

	public void resizeSelectorToFocus(Rect paramRect, boolean paramBoolean)
  {
    int i = 1;
    float f1 = paramRect.width() / this.mSelector.getWidth();
    float f2 = paramRect.height() / this.mSelector.getHeight();
    if (paramBoolean)
    {
      float f5 = this.mContext.getResources().getFraction(2131230721, i, i);
      f1 *= f5;
      f2 *= f5;
    }
    float f3 = Math.max(f1, f2);
    float f4 = Math.min(f1, f2);
    if (!Float.isNaN(f3)) {
      if (f3 / f4 >= 1.1D) {
        break label165;
      }
    }
    for (;;)
    {
      setSelectorType(i);
      if (i != 0)
      {
        f1 = f3;
        f2 = f3;
      }
      this.mSelector.clearAnimation();
      this.mSelector.animate().setDuration(150L).scaleX(f1).scaleY(f2).setInterpolator(sMovementInterpolator).start();
      return;
      label165:
      int j = 0;
    }
  }

	public void setDismissListener(DismissListener paramDismissListener) {
		this.mDismissListener = paramDismissListener;
	}

	public void setFocus(KeyFocus paramKeyFocus) {
		setKbFocus(paramKeyFocus, false);
	}

	public void setTouchState(int paramInt) {
		switch (paramInt) {
		}
		for (;;) {
			setTouchStateInternal(paramInt);
			setKbFocus(this.mCurrKeyInfo, true);
			return;
			if ((this.mTouchState == 2) || (this.mTouchState == 3)) {
				this.mTouchIndicatorAnimator.reverse();
			}
			if (this.mTouchState == 3) {
				this.mKeySelector.animate().alpha(0.0F)
						.setDuration(this.mClickAnimDur);
				continue;
				if (this.mTouchState == 3) {
					this.mKeySelector.animate().alpha(0.0F)
							.setDuration(this.mClickAnimDur);
					this.mTouchIndicatorAnimator.reverse();
				} else if (this.mTouchState == 2) {
					this.mTouchIndicatorAnimator.reverse();
					continue;
					if ((this.mTouchState == 0) || (this.mTouchState == 1)) {
						this.mTouchIndicatorAnimator.start();
						continue;
						if ((this.mTouchState == 0) || (this.mTouchState == 1)) {
							this.mTouchIndicatorAnimator.start();
						}
						this.mKeySelector.animate().alpha(1.0F)
								.setDuration(this.mClickAnimDur);
					}
				}
			}
		}
	}

	public void setVoiceListener(VoiceListener paramVoiceListener) {
		this.mVoiceListener = paramVoiceListener;
	}

	public void startVoiceRecording() {
		if (this.mVoiceEnabled) {
			if (this.mVoiceKeyDismissesEnabled) {
				this.mDismissListener.onDismiss(true);
			}
		} else {
			return;
		}
		this.mVoiceAnimator.startEnterAnimation();
	}

	public void updateSuggestions(ArrayList<String> paramArrayList) {
		int i = this.mSuggestions.getChildCount();
		int j = paramArrayList.size();
		if (j < i) {
			this.mSuggestions.removeViews(j, i - j);
		}
		for (;;) {
			for (int m = 0; m < j; m++) {
				((Button) this.mSuggestions.getChildAt(m).findViewById(
						2131558401)).setText((CharSequence) paramArrayList
						.get(m));
			}
			if (j > i) {
				for (int k = i; k < j; k++) {
					View localView = this.mContext.getLayoutInflater().inflate(
							2130903042, null);
					this.mSuggestions.addView(localView);
				}
			}
		}
		if (getCurrFocus().type == 3) {
			resetFocusCursor(true);
		}
	}

	public static abstract interface DismissListener {
		public abstract void onDismiss(boolean paramBoolean);
	}

	public static class KeyFocus {
		public static final int TYPE_ACTION = 2;
		public static final int TYPE_INVALID = -1;
		public static final int TYPE_MAIN = 0;
		public static final int TYPE_SUGGESTION = 3;
		public static final int TYPE_VOICE = 1;
		int code;
		int index;
		CharSequence label;
		final Rect rect = new Rect();
		int type = -1;

		public boolean equals(Object paramObject) {
			if (this == paramObject) {
			}
			KeyFocus localKeyFocus;
			do {
				return true;
				if ((paramObject == null)
						|| (getClass() != paramObject.getClass())) {
					return false;
				}
				localKeyFocus = (KeyFocus) paramObject;
				if (this.code != localKeyFocus.code) {
					return false;
				}
				if (this.index != localKeyFocus.index) {
					return false;
				}
				if (this.type != localKeyFocus.type) {
					return false;
				}
				if (this.label != null) {
					if (this.label.equals(localKeyFocus.label)) {
					}
				} else {
					while (localKeyFocus.label != null) {
						return false;
					}
				}
			} while (this.rect.equals(localKeyFocus.rect));
			return false;
		}

		public int hashCode() {
			int i = 31 * (31 * (31 * (31 * this.rect.hashCode() + this.index) + this.type) + this.code);
			if (this.label != null) {
			}
			for (int j = this.label.hashCode();; j = 0) {
				return i + j;
			}
		}

		public void set(KeyFocus paramKeyFocus) {
			this.index = paramKeyFocus.index;
			this.type = paramKeyFocus.type;
			this.code = paramKeyFocus.code;
			this.label = paramKeyFocus.label;
			this.rect.set(paramKeyFocus.rect);
		}

		public String toString() {
			StringBuilder localStringBuilder = new StringBuilder();
			localStringBuilder.append("[type: ").append(this.type)
					.append(", index: ").append(this.index).append(", code: ")
					.append(this.code).append(", label: ").append(this.label)
					.append(", rect: ").append(this.rect).append("]");
			return localStringBuilder.toString();
		}
	}

	private class VoiceIntroAnimator {
		private Animator.AnimatorListener mEnterListener;
		private Animator.AnimatorListener mExitListener;
		private ValueAnimator mValueAnimator;

		public VoiceIntroAnimator(
				Animator.AnimatorListener paramAnimatorListener1,
				Animator.AnimatorListener paramAnimatorListener2) {
			this.mEnterListener = paramAnimatorListener1;
			this.mExitListener = paramAnimatorListener2;
			float[] arrayOfFloat = new float[2];
			arrayOfFloat[0] = LeanbackKeyboardContainer.this.mAlphaOut;
			arrayOfFloat[1] = LeanbackKeyboardContainer.this.mAlphaIn;
			this.mValueAnimator = ValueAnimator.ofFloat(arrayOfFloat);
			this.mValueAnimator
					.setDuration(LeanbackKeyboardContainer.this.mVoiceAnimDur);
			this.mValueAnimator.setInterpolator(new AccelerateInterpolator());
		}

		private void start(final boolean paramBoolean) {
			this.mValueAnimator.cancel();
			this.mValueAnimator.removeAllListeners();
			ValueAnimator localValueAnimator = this.mValueAnimator;
			if (paramBoolean) {
			}
			for (Animator.AnimatorListener localAnimatorListener = this.mEnterListener;; localAnimatorListener = this.mExitListener) {
				localValueAnimator.addListener(localAnimatorListener);
				this.mValueAnimator.removeAllUpdateListeners();
				this.mValueAnimator
						.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
							public void onAnimationUpdate(
									ValueAnimator paramAnonymousValueAnimator) {
								float f1 = ((Float) LeanbackKeyboardContainer.VoiceIntroAnimator.this.mValueAnimator
										.getAnimatedValue()).floatValue();
								float f2 = LeanbackKeyboardContainer.this.mAlphaIn
										+ LeanbackKeyboardContainer.this.mAlphaOut
										- f1;
								float f3;
								float f4;
								if (paramBoolean) {
									f3 = f2;
									if (!paramBoolean) {
										break label149;
									}
									f4 = f1;
									label61: LeanbackKeyboardContainer.this.mMainKeyboardView
											.setAlpha(f3);
									LeanbackKeyboardContainer.this.mActionButtonView
											.setAlpha(f3);
									LeanbackKeyboardContainer.this.mVoiceButtonView
											.setAlpha(f4);
									if (f1 != LeanbackKeyboardContainer.this.mAlphaOut) {
										break label184;
									}
									if (!paramBoolean) {
										break label155;
									}
									LeanbackKeyboardContainer.this.mVoiceButtonView
											.setVisibility(0);
								}
								label149: label155: label184: while (f1 != LeanbackKeyboardContainer.this.mAlphaIn) {
									return;
									f3 = f1;
									break;
									f4 = f2;
									break label61;
									LeanbackKeyboardContainer.this.mMainKeyboardView
											.setVisibility(0);
									LeanbackKeyboardContainer.this.mActionButtonView
											.setVisibility(0);
									return;
								}
								if (paramBoolean) {
									LeanbackKeyboardContainer.this.mMainKeyboardView
											.setVisibility(4);
									LeanbackKeyboardContainer.this.mActionButtonView
											.setVisibility(4);
									return;
								}
								LeanbackKeyboardContainer.this.mVoiceButtonView
										.setVisibility(4);
							}
						});
				this.mValueAnimator.start();
				return;
			}
		}

		void startEnterAnimation() {
			if ((!LeanbackKeyboardContainer.this.isVoiceVisible())
					&& (!this.mValueAnimator.isRunning())) {
				start(true);
			}
		}

		void startExitAnimation() {
			if ((LeanbackKeyboardContainer.this.isVoiceVisible())
					&& (!this.mValueAnimator.isRunning())) {
				start(false);
			}
		}
	}

	public static abstract interface VoiceListener {
		public abstract void onVoiceResult(String paramString);
	}
}

/*
 * Location: C:\Users\Administrator\Desktop\Leanback_Keyboard_L-
 * 1236599_dex2jar.jar!\com\google\leanback\ime\LeanbackKeyboardContainer.class
 * Java compiler version: 6 (50.0) JD-Core Version: 0.7.1-SNAPSHOT-20140817
 */