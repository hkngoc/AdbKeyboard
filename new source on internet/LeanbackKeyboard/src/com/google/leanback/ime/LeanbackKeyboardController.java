package com.google.leanback.ime;

import android.content.res.Resources;
import android.graphics.PointF;
import android.graphics.Rect;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard.Key;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.inputmethod.EditorInfo;
import android.widget.RelativeLayout;
import com.google.android.pano.util.TouchNavSpaceTracker;
import com.google.android.pano.util.TouchNavSpaceTracker.KeyEventListener;
import com.google.android.pano.util.TouchNavSpaceTracker.PhysicalMotionEvent;
import com.google.android.pano.util.TouchNavSpaceTracker.TouchEventListener;
import java.util.ArrayList;

public class LeanbackKeyboardController
  implements LeanbackKeyboardContainer.VoiceListener, LeanbackKeyboardContainer.DismissListener
{
  public static final int CLICK_MOVEMENT_BLOCK_DURATION_MS = 500;
  private static final boolean DEBUG = false;
  private static final float FOCUS_CHANGE_RATIO = 2.0F;
  private static final int KEY_CHANGE_HISTORY_SIZE = 10;
  private static final long KEY_CHANGE_REVERT_TIME_MS = 100L;
  private static final int MSG_SNAP_TO_KEY = 0;
  private static final long SNAP_TIMEOUT = 350L;
  private static final String TAG = "LbKbController";
  private LeanbackKeyboardContainer mContainer;
  private InputMethodService mContext;
  private DoubleClickDetector mDoubleClickDetector = new DoubleClickDetector(null);
  private LeanbackKeyboardContainer.KeyFocus mDownFocus = new LeanbackKeyboardContainer.KeyFocus();
  private Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      int i = 1;
      switch (paramAnonymousMessage.what)
      {
      default: 
        return;
      }
      PointF localPointF = LeanbackKeyboardController.this.getCurrentKeyPosition();
      LeanbackKeyboardController.this.mContainer.alignSelector(localPointF.x, localPointF.y, i);
      LeanbackKeyboardContainer localLeanbackKeyboardContainer = LeanbackKeyboardController.this.mContainer;
      if (LeanbackKeyboardController.this.mTouchDownReceived) {}
      for (;;)
      {
        localLeanbackKeyboardContainer.setTouchState(i);
        LeanbackKeyboardController.this.updatePositionToCurrentFocus();
        return;
        int j = 0;
      }
    }
  };
  private InputListener mInputListener;
  ArrayList<KeyChange> mKeyChangeHistory = new ArrayList(11);
  private int mKeyDownKeyCode;
  private LeanbackKeyboardContainer.KeyFocus mKeyDownKeyFocus;
  private boolean mKeyDownReceived = false;
  private boolean mLongPressHandled = false;
  private int mMoveCount;
  private View.OnLayoutChangeListener mOnLayoutChangeListener = new View.OnLayoutChangeListener()
  {
    public void onLayoutChange(View paramAnonymousView, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3, int paramAnonymousInt4, int paramAnonymousInt5, int paramAnonymousInt6, int paramAnonymousInt7, int paramAnonymousInt8)
    {
      int i = paramAnonymousInt3 - paramAnonymousInt1;
      int j = paramAnonymousInt4 - paramAnonymousInt2;
      int k = paramAnonymousInt7 - paramAnonymousInt5;
      int m = paramAnonymousInt8 - paramAnonymousInt6;
      if ((i > 0) && (j > 0))
      {
        if (LeanbackKeyboardController.this.mSpaceTracker != null) {
          LeanbackKeyboardController.this.mSpaceTracker.setPixelSize(i, j);
        }
        if ((i != k) || (j != m)) {
          LeanbackKeyboardController.this.initInputView();
        }
      }
    }
  };
  public float mResizeSquareDistance;
  private TouchNavSpaceTracker mSpaceTracker;
  private LeanbackKeyboardContainer.KeyFocus mTempFocus = new LeanbackKeyboardContainer.KeyFocus();
  private PointF mTempPoint = new PointF();
  private boolean mTouchDownReceived = false;
  private TouchEventListener mTouchEventListener = new TouchEventListener(null);
  
  public LeanbackKeyboardController(InputMethodService paramInputMethodService, InputListener paramInputListener)
  {
    this(paramInputMethodService, paramInputListener, new TouchNavSpaceTracker(), new LeanbackKeyboardContainer(paramInputMethodService));
  }
  
  LeanbackKeyboardController(InputMethodService paramInputMethodService, InputListener paramInputListener, TouchNavSpaceTracker paramTouchNavSpaceTracker, LeanbackKeyboardContainer paramLeanbackKeyboardContainer)
  {
    this.mContext = paramInputMethodService;
    this.mResizeSquareDistance = paramInputMethodService.getResources().getDimension(2131296290);
    this.mResizeSquareDistance *= this.mResizeSquareDistance;
    this.mInputListener = paramInputListener;
    setSpaceTracker(paramTouchNavSpaceTracker);
    setKeyboardContainer(paramLeanbackKeyboardContainer);
    this.mContainer.setVoiceListener(this);
    this.mContainer.setDismissListener(this);
  }
  
  private void clearKeyIfNecessary()
  {
    this.mMoveCount = (1 + this.mMoveCount);
    if (this.mMoveCount >= 3)
    {
      this.mMoveCount = 0;
      this.mKeyDownKeyFocus = null;
    }
  }
  
  private void commitKey()
  {
    commitKey(this.mContainer.getCurrFocus());
  }
  
  private void commitKey(LeanbackKeyboardContainer.KeyFocus paramKeyFocus)
  {
    if ((this.mContainer == null) || (paramKeyFocus == null)) {}
    Keyboard.Key localKey;
    do
    {
      return;
      switch (paramKeyFocus.type)
      {
      default: 
        localKey = this.mContainer.getKey(paramKeyFocus.type, paramKeyFocus.index);
      }
    } while (localKey == null);
    handleCommitKeyboardKey(localKey.codes[0], localKey.label);
    return;
    this.mContainer.onVoiceClick();
    return;
    this.mInputListener.onEntry(5, 0, null);
    return;
    this.mInputListener.onEntry(2, 0, this.mContainer.getSuggestionText(paramKeyFocus.index));
    this.mContainer.resetFocusCursor(true);
  }
  
  private PointF getBestSnapPosition(PointF paramPointF, long paramLong)
  {
    if (this.mKeyChangeHistory.size() <= 1) {
      return paramPointF;
    }
    for (int i = 0;; i++) {
      if (i < -1 + this.mKeyChangeHistory.size())
      {
        KeyChange localKeyChange = (KeyChange)this.mKeyChangeHistory.get(i);
        if (paramLong - ((KeyChange)this.mKeyChangeHistory.get(i + 1)).time < 100L)
        {
          paramPointF = localKeyChange.position;
          this.mKeyChangeHistory.clear();
          this.mKeyChangeHistory.add(new KeyChange(paramLong, paramPointF));
        }
      }
      else
      {
        return paramPointF;
      }
    }
  }
  
  private PointF getCurrentKeyPosition()
  {
    if (this.mContainer != null)
    {
      LeanbackKeyboardContainer.KeyFocus localKeyFocus = this.mContainer.getCurrFocus();
      return new PointF(localKeyFocus.rect.centerX(), localKeyFocus.rect.centerY());
    }
    return null;
  }
  
  private int getSimplifiedKey(int paramInt)
  {
    if ((paramInt == 23) || (paramInt == 160) || (paramInt == 96)) {
      paramInt = 23;
    }
    if (paramInt == 97) {
      paramInt = 4;
    }
    return paramInt;
  }
  
  private void handleCommitKeyboardKey(int paramInt, CharSequence paramCharSequence)
  {
    switch (paramInt)
    {
    default: 
      if ((this.mContainer.isCurrKeyShifted()) && (paramCharSequence != null) && (paramCharSequence.length() < 3) && (Character.isLowerCase(paramCharSequence.charAt(0))))
      {
        paramCharSequence = paramCharSequence.toString().toUpperCase();
        paramInt = paramCharSequence.charAt(0);
      }
      this.mInputListener.onEntry(0, paramInt, paramCharSequence);
      this.mContainer.onTextEntry();
      if (this.mContainer.isMiniKeyboardOnScreen()) {
        this.mContainer.onDismissMiniKeyboard();
      }
    case -2: 
    case 297: 
    case -1: 
    case 296: 
      do
      {
        return;
        if (Log.isLoggable("LbKbController", 2)) {
          Log.d("LbKbController", "mode change");
        }
        this.mContainer.onModeChangeClick();
        return;
        this.mContainer.onShiftDoubleClick(this.mContainer.isCapsLockOn());
        return;
        if (Log.isLoggable("LbKbController", 2)) {
          Log.d("LbKbController", "shift");
        }
        this.mContainer.onShiftClick();
        return;
      } while (this.mKeyDownKeyCode != 296);
      this.mContainer.onDismissMiniKeyboard();
      return;
    case 295: 
      this.mInputListener.onEntry(3, 0, null);
      return;
    case 294: 
      this.mInputListener.onEntry(4, 0, null);
      return;
    case -5: 
      this.mInputListener.onEntry(1, 0, null);
      return;
    case 32: 
      this.mInputListener.onEntry(0, paramInt, " ");
      this.mContainer.onSpaceEntry();
      return;
    case 46: 
      this.mInputListener.onEntry(0, paramInt, paramCharSequence);
      this.mContainer.onPeriodEntry();
      return;
    }
    this.mContainer.startVoiceRecording();
  }
  
  private boolean handleKeyDownEvent(int paramInt1, int paramInt2)
  {
    int i = getSimplifiedKey(paramInt1);
    boolean bool;
    if (i == 4)
    {
      this.mContainer.cancelVoiceRecording();
      bool = false;
    }
    for (;;)
    {
      return bool;
      if (this.mContainer.isVoiceVisible())
      {
        if ((i == 22) || (i == 23)) {
          this.mContainer.cancelVoiceRecording();
        }
        return true;
      }
      bool = true;
      switch (i)
      {
      case 106: 
      case 107: 
      default: 
        return false;
      case 21: 
        return onDirectionalMove(1, true);
      case 22: 
        return onDirectionalMove(4, true);
      case 19: 
        return onDirectionalMove(8, true);
      case 20: 
        return onDirectionalMove(2, true);
      case 99: 
        handleCommitKeyboardKey(-5, null);
        return bool;
      case 100: 
        handleCommitKeyboardKey(32, null);
        return bool;
      case 102: 
        handleCommitKeyboardKey(295, null);
        return bool;
      case 103: 
        handleCommitKeyboardKey(294, null);
        return bool;
      }
      if (paramInt2 == 0)
      {
        this.mKeyDownKeyCode = this.mContainer.getCurrKeyCode();
        this.mMoveCount = 0;
        this.mKeyDownKeyFocus = new LeanbackKeyboardContainer.KeyFocus();
        this.mKeyDownKeyFocus.set(this.mContainer.getCurrFocus());
      }
      while (isKeyHandledOnKeyDown(this.mContainer.getCurrKeyCode()))
      {
        commitKey();
        return bool;
        if (paramInt2 == 1) {
          handleKeyLongPress(i);
        }
      }
    }
  }
  
  private boolean handleKeyLongPress(int paramInt)
  {
    if ((isEnterKey(paramInt)) && (this.mContainer.onKeyLongPress())) {}
    for (boolean bool = true;; bool = false)
    {
      this.mLongPressHandled = bool;
      if (this.mContainer.isMiniKeyboardOnScreen()) {
        Log.d("LbKbController", "mini keyboard shown after long press");
      }
      return this.mLongPressHandled;
    }
  }
  
  private boolean handleKeyUpEvent(int paramInt, long paramLong)
  {
    int i = getSimplifiedKey(paramInt);
    boolean bool;
    if (i == 4) {
      bool = false;
    }
    do
    {
      return bool;
      if (this.mContainer.isVoiceVisible()) {
        return true;
      }
      bool = true;
      switch (i)
      {
      case 99: 
      case 100: 
      case 102: 
      case 103: 
      default: 
        return false;
      case 19: 
      case 20: 
      case 21: 
      case 22: 
        clearKeyIfNecessary();
        return bool;
      case 23: 
        if (this.mContainer.getCurrKeyCode() == -1)
        {
          this.mDoubleClickDetector.addEvent(paramLong);
          return bool;
        }
        break;
      }
    } while (isKeyHandledOnKeyDown(this.mContainer.getCurrKeyCode()));
    commitKey(this.mKeyDownKeyFocus);
    return bool;
    handleCommitKeyboardKey(-2, null);
    return bool;
    handleCommitKeyboardKey(297, null);
    return bool;
  }
  
  private void handleMovementDown()
  {
    this.mTouchDownReceived = true;
    updatePositionToCurrentFocus();
    this.mDownFocus.set(this.mContainer.getCurrFocus());
    this.mContainer.setTouchState(1);
  }
  
  private void initInputView()
  {
    this.mContainer.onInitInputView();
    updatePositionToCurrentFocus();
  }
  
  private boolean isEnterKey(int paramInt)
  {
    return getSimplifiedKey(paramInt) == 23;
  }
  
  private boolean isKeyHandledOnKeyDown(int paramInt)
  {
    return (paramInt == -5) || (paramInt == 295) || (paramInt == 294);
  }
  
  private boolean onDirectionalMove(int paramInt, boolean paramBoolean)
  {
    this.mContainer.getNextFocusInDirection(paramInt, this.mDownFocus, this.mTempFocus);
    this.mContainer.setFocus(this.mTempFocus);
    this.mDownFocus.set(this.mTempFocus);
    if (paramBoolean) {
      this.mContainer.alignSelectorToFocus(true);
    }
    clearKeyIfNecessary();
    return true;
  }
  
  private void performBestSnap(long paramLong)
  {
    LeanbackKeyboardContainer.KeyFocus localKeyFocus = this.mContainer.getCurrFocus();
    this.mTempPoint.x = localKeyFocus.rect.centerX();
    this.mTempPoint.y = localKeyFocus.rect.centerY();
    PointF localPointF = getBestSnapPosition(this.mTempPoint, paramLong);
    this.mContainer.getBestFocus(localPointF.x, localPointF.y, this.mTempFocus);
    this.mContainer.setFocus(this.mTempFocus);
    this.mContainer.alignSelector(localPointF.x, localPointF.y, true);
    updatePositionToCurrentFocus();
  }
  
  private void updatePositionToCurrentFocus()
  {
    PointF localPointF = getCurrentKeyPosition();
    if ((localPointF != null) && (this.mSpaceTracker != null)) {
      this.mSpaceTracker.setPixelPosition(localPointF.x, localPointF.y);
    }
  }
  
  public boolean areSuggestionsEnabled()
  {
    if (this.mContainer != null) {
      return this.mContainer.areSuggestionsEnabled();
    }
    return false;
  }
  
  public boolean enableAutoEnterSpace()
  {
    if (this.mContainer != null) {
      return this.mContainer.enableAutoEnterSpace();
    }
    return false;
  }
  
  public View getView()
  {
    if (this.mContainer != null) {
      return this.mContainer.getView();
    }
    return null;
  }
  
  public void onDismiss(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mInputListener.onEntry(8, 0, null);
      return;
    }
    this.mInputListener.onEntry(7, 0, null);
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent)
  {
    return (this.mSpaceTracker != null) && (this.mContext != null) && (this.mContext.isInputViewShown()) && (this.mSpaceTracker.onGenericMotionEvent(paramMotionEvent));
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    this.mDownFocus.set(this.mContainer.getCurrFocus());
    if ((this.mSpaceTracker != null) && (this.mSpaceTracker.onKeyDown(paramInt, paramKeyEvent))) {
      return true;
    }
    if (isEnterKey(paramInt))
    {
      this.mKeyDownReceived = true;
      if (paramKeyEvent.getRepeatCount() == 0) {
        this.mContainer.setTouchState(3);
      }
    }
    return handleKeyDownEvent(paramInt, paramKeyEvent.getRepeatCount());
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((this.mSpaceTracker != null) && (this.mSpaceTracker.onKeyUp(paramInt, paramKeyEvent))) {
      return true;
    }
    if (isEnterKey(paramInt))
    {
      if ((!this.mKeyDownReceived) || (this.mLongPressHandled))
      {
        this.mLongPressHandled = false;
        return true;
      }
      this.mKeyDownReceived = false;
      if (this.mContainer.getTouchState() == 3) {
        this.mContainer.setTouchState(1);
      }
    }
    return handleKeyUpEvent(paramInt, paramKeyEvent.getEventTime());
  }
  
  public void onStartInput(EditorInfo paramEditorInfo)
  {
    if (this.mContainer != null)
    {
      this.mContainer.onStartInput(paramEditorInfo);
      initInputView();
    }
  }
  
  public void onStartInputView()
  {
    this.mKeyDownReceived = false;
    this.mTouchDownReceived = false;
    if (this.mContainer != null) {
      this.mContainer.onStartInputView();
    }
    this.mDoubleClickDetector.reset();
  }
  
  public void onVoiceResult(String paramString)
  {
    this.mInputListener.onEntry(6, 0, paramString);
  }
  
  public void setKeyboardContainer(LeanbackKeyboardContainer paramLeanbackKeyboardContainer)
  {
    this.mContainer = paramLeanbackKeyboardContainer;
    paramLeanbackKeyboardContainer.getView().addOnLayoutChangeListener(this.mOnLayoutChangeListener);
  }
  
  public void setSpaceTracker(TouchNavSpaceTracker paramTouchNavSpaceTracker)
  {
    this.mSpaceTracker = paramTouchNavSpaceTracker;
    paramTouchNavSpaceTracker.setLPFEnabled(true);
    paramTouchNavSpaceTracker.setKeyEventListener(this.mTouchEventListener);
    paramTouchNavSpaceTracker.setTouchEventListener(this.mTouchEventListener);
  }
  
  public void updateSuggestions(ArrayList<String> paramArrayList)
  {
    if (this.mContainer != null) {
      this.mContainer.updateSuggestions(paramArrayList);
    }
  }
  
  private class DoubleClickDetector
  {
    final long DOUBLE_CLICK_TIMEOUT_MS = 200L;
    boolean mFirstClickShiftLocked;
    long mFirstClickTime = 0L;
    
    private DoubleClickDetector() {}
    
    public void addEvent(long paramLong)
    {
      if (paramLong - this.mFirstClickTime > 200L)
      {
        this.mFirstClickTime = paramLong;
        this.mFirstClickShiftLocked = LeanbackKeyboardController.this.mContainer.isCapsLockOn();
        LeanbackKeyboardController.this.commitKey();
        return;
      }
      LeanbackKeyboardController.this.mContainer.onShiftDoubleClick(this.mFirstClickShiftLocked);
      reset();
    }
    
    public void reset()
    {
      this.mFirstClickTime = 0L;
    }
  }
  
  public static abstract interface InputListener
  {
    public static final int ENTRY_TYPE_ACTION = 5;
    public static final int ENTRY_TYPE_BACKSPACE = 1;
    public static final int ENTRY_TYPE_DISMISS = 7;
    public static final int ENTRY_TYPE_LEFT = 3;
    public static final int ENTRY_TYPE_RIGHT = 4;
    public static final int ENTRY_TYPE_STRING = 0;
    public static final int ENTRY_TYPE_SUGGESTION = 2;
    public static final int ENTRY_TYPE_VOICE = 6;
    public static final int ENTRY_TYPE_VOICE_DISMISS = 8;
    
    public abstract void onEntry(int paramInt1, int paramInt2, CharSequence paramCharSequence);
  }
  
  private static final class KeyChange
  {
    public PointF position;
    public long time;
    
    public KeyChange(long paramLong, PointF paramPointF)
    {
      this.time = paramLong;
      this.position = paramPointF;
    }
  }
  
  private class TouchEventListener
    implements TouchNavSpaceTracker.KeyEventListener, TouchNavSpaceTracker.TouchEventListener
  {
    private TouchEventListener() {}
    
    public boolean onDown(TouchNavSpaceTracker.PhysicalMotionEvent paramPhysicalMotionEvent)
    {
      LeanbackKeyboardController.this.handleMovementDown();
      return true;
    }
    
    public boolean onFlick(TouchNavSpaceTracker.PhysicalMotionEvent paramPhysicalMotionEvent1, TouchNavSpaceTracker.PhysicalMotionEvent paramPhysicalMotionEvent2, int paramInt1, int paramInt2)
    {
      return LeanbackKeyboardController.this.onDirectionalMove(paramInt1, false);
    }
    
    public boolean onFling(TouchNavSpaceTracker.PhysicalMotionEvent paramPhysicalMotionEvent1, TouchNavSpaceTracker.PhysicalMotionEvent paramPhysicalMotionEvent2, float paramFloat1, float paramFloat2)
    {
      return false;
    }
    
    public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
    {
      if (LeanbackKeyboardController.this.isEnterKey(paramInt))
      {
        LeanbackKeyboardController.access$102(LeanbackKeyboardController.this, true);
        if (paramKeyEvent.getRepeatCount() == 0)
        {
          LeanbackKeyboardController.this.mContainer.setTouchState(3);
          LeanbackKeyboardController.this.mSpaceTracker.blockMovementUntil(500L + paramKeyEvent.getEventTime());
          LeanbackKeyboardController.this.performBestSnap(paramKeyEvent.getEventTime());
        }
      }
      return LeanbackKeyboardController.this.handleKeyDownEvent(paramInt, paramKeyEvent.getRepeatCount());
    }
    
    public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent)
    {
      return LeanbackKeyboardController.this.handleKeyLongPress(paramInt);
    }
    
    public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
    {
      if (LeanbackKeyboardController.this.isEnterKey(paramInt))
      {
        if ((!LeanbackKeyboardController.this.mKeyDownReceived) || (LeanbackKeyboardController.this.mLongPressHandled))
        {
          LeanbackKeyboardController.access$602(LeanbackKeyboardController.this, false);
          return true;
        }
        LeanbackKeyboardController.access$102(LeanbackKeyboardController.this, false);
        if (LeanbackKeyboardController.this.mContainer.getTouchState() == 3)
        {
          LeanbackKeyboardController.this.mContainer.setTouchState(1);
          LeanbackKeyboardController.this.mSpaceTracker.unblockMovement();
        }
      }
      return LeanbackKeyboardController.this.handleKeyUpEvent(paramInt, paramKeyEvent.getEventTime());
    }
    
    public boolean onMove(TouchNavSpaceTracker.PhysicalMotionEvent paramPhysicalMotionEvent1, TouchNavSpaceTracker.PhysicalMotionEvent paramPhysicalMotionEvent2, float paramFloat1, float paramFloat2)
    {
      if (LeanbackKeyboardController.this.mContainer.isVoiceVisible()) {
        return true;
      }
      if (!LeanbackKeyboardController.this.mTouchDownReceived)
      {
        LeanbackKeyboardController.this.handleMovementDown();
        return true;
      }
      LeanbackKeyboardController.this.mContainer.getBestFocus(paramFloat1, paramFloat2, LeanbackKeyboardController.this.mTempFocus);
      LeanbackKeyboardContainer.KeyFocus localKeyFocus = LeanbackKeyboardController.this.mContainer.getCurrFocus();
      LeanbackKeyboardController.this.mContainer.alignSelector(paramFloat1, paramFloat2, false);
      float f1 = paramFloat1 - LeanbackKeyboardController.this.mTempFocus.rect.centerX();
      float f2 = paramFloat2 - LeanbackKeyboardController.this.mTempFocus.rect.centerY();
      float f3 = f1 * f1 + f2 * f2;
      if ((LeanbackKeyboardController.this.mContainer.getTouchState() == 1) && (f3 > LeanbackKeyboardController.this.mResizeSquareDistance)) {
        LeanbackKeyboardController.this.mContainer.setTouchState(2);
      }
      if (!localKeyFocus.equals(LeanbackKeyboardController.this.mTempFocus))
      {
        float f4 = Math.abs(paramFloat1 - localKeyFocus.rect.centerX());
        float f5 = Math.abs(paramFloat2 - localKeyFocus.rect.centerY());
        float f6 = Math.abs(paramFloat1 - LeanbackKeyboardController.this.mTempFocus.rect.centerX());
        float f7 = Math.abs(paramFloat2 - LeanbackKeyboardController.this.mTempFocus.rect.centerY());
        float f8 = f4 / f6;
        float f9 = f5 / f7;
        if ((f8 > 2.0F) || (f9 > 2.0F)) {
          LeanbackKeyboardController.this.mContainer.setFocus(LeanbackKeyboardController.this.mTempFocus);
        }
        LeanbackKeyboardController.this.mKeyChangeHistory.add(new LeanbackKeyboardController.KeyChange(paramPhysicalMotionEvent2.getTime(), LeanbackKeyboardController.this.getCurrentKeyPosition()));
        LeanbackKeyboardController.this.mHandler.removeMessages(0);
        LeanbackKeyboardController.this.mHandler.sendEmptyMessageDelayed(0, 350L);
      }
      return true;
    }
    
    public boolean onUp(TouchNavSpaceTracker.PhysicalMotionEvent paramPhysicalMotionEvent, float paramFloat1, float paramFloat2)
    {
      LeanbackKeyboardController.access$902(LeanbackKeyboardController.this, false);
      LeanbackKeyboardController.this.mContainer.alignSelectorToFocus(true);
      LeanbackKeyboardController.this.mContainer.setTouchState(0);
      LeanbackKeyboardController.this.mHandler.removeMessages(0);
      return true;
    }
  }
}


/* Location:              C:\Users\Administrator\Desktop\Leanback_Keyboard_L-1236599_dex2jar.jar!\com\google\leanback\ime\LeanbackKeyboardController.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1-SNAPSHOT-20140817
 */