package com.google.leanback.ime;

import android.view.inputmethod.EditorInfo;

public class LeanbackUtils {
	public static int getImeAction(EditorInfo paramEditorInfo) {
		return 0x400000FF & paramEditorInfo.imeOptions;
	}

	public static int getInputTypeClass(EditorInfo paramEditorInfo) {
		return 0xF & paramEditorInfo.inputType;
	}

	public static int getInputTypeVariation(EditorInfo paramEditorInfo) {
		return 0xFF0 & paramEditorInfo.inputType;
	}

	public static boolean isAlphabet(int paramInt) {
		return Character.isLetter(paramInt);
	}
}
