package com.google.leanback.ime;

import android.util.EventLog;

public class EventLogTags {
	public static final int TIME_LEANBACK_IME_INPUT = 270900;
	public static final int TOTAL_LEANBACK_IME_BACKSPACE = 270902;

	public static void writeTimeLeanbackImeInput(long paramLong1,
			long paramLong2) {
		Object[] arrayOfObject = new Object[2];
		arrayOfObject[0] = Long.valueOf(paramLong1);
		arrayOfObject[1] = Long.valueOf(paramLong2);
		EventLog.writeEvent(270900, arrayOfObject);
	}

	public static void writeTotalLeanbackImeBackspace(int paramInt) {
		EventLog.writeEvent(270902, paramInt);
	}
}
