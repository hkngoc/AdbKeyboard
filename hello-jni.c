/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/hellojni/HelloJni.java
 */
jstring Java_com_example_hellojni_HelloJni_stringFromJNI(JNIEnv* env,
		jobject thiz) {
#if defined(__arm__)
#if defined(__ARM_ARCH_7A__)
#if defined(__ARM_NEON__)
#if defined(__ARM_PCS_VFP)
#define ABI "armeabi-v7a/NEON (hard-float)"
#else
#define ABI "armeabi-v7a/NEON"
#endif
#else
#if defined(__ARM_PCS_VFP)
#define ABI "armeabi-v7a (hard-float)"
#else
#define ABI "armeabi-v7a"
#endif
#endif
#else
#define ABI "armeabi"
#endif
#elif defined(__i386__)
#define ABI "x86"
#elif defined(__x86_64__)
#define ABI "x86_64"
#elif defined(__mips64)  /* mips64el-* toolchain defines __mips__ too */
#define ABI "mips64"
#elif defined(__mips__)
#define ABI "mips"
#elif defined(__aarch64__)
#define ABI "arm64-v8a"
#else
#define ABI "unknown"
#endif
	return (*env)->NewStringUTF(env,
			"Hello from JNI !  Compiled with ABI " ABI ".");
}

struct fb_fix_screeninfo FixedInfo;
struct fb_var_screeninfo OrigVarInfo;

int Java_com_example_hellojni_HelloJni_imageFromJNI() {
//#ifndef __ANDROID__
//#define FRAMEBUFFER "/dev/fb0"
//#else
//#define FRAMEBUFFER "/dev/graphics/fb0"
//#endif //__ANDROID__
#define FRAMEBUFFER "/dev/graphics/fb0"
	int FrameBufferFD = -5;
	void *FrameBuffer = (void *) -1;
	/* open the framebuffer device */
	FrameBufferFD = open(FRAMEBUFFER, O_RDWR);
	if (FrameBufferFD < 0) {
		perror("cannot open fb0");
		return -1;
	}
	/* Get the fixed screen info */
	if (ioctl(FrameBufferFD, FBIOGET_FSCREENINFO, &FixedInfo)) {
		perror("failed to get fb0 info");
		return -2;
	}
	/* get the variable screen info */
	if (ioctl(FrameBufferFD, FBIOGET_VSCREENINFO, &OrigVarInfo)) {
		perror("failed to get fb0 info");
		return -3;
	}

	if (FixedInfo.visual != FB_VISUAL_TRUECOLOR
			&& FixedInfo.visual != FB_VISUAL_DIRECTCOLOR) {
		perror("failed to get fb0 info");
		return -4;
	}
	/*
	 * fbdev says the frame buffer is at offset zero, and the mmio region
	 * is immediately after.
	 */
	/* mmap the framebuffer into our address space */
	FrameBuffer = (void *) mmap(0, FixedInfo.smem_len, PROT_READ | PROT_WRITE,
	MAP_SHARED, FrameBufferFD, 0);
	if (FrameBuffer == (void *) -1) {
		perror("failed to mmap framebuffer");
		return -1;
	}
	return FrameBuffer;
//	munmap(FrameBuffer, FixedInfo.smem_len);
//	close(FrameBufferFD);
}

http://www.pocketmagic.net/android-native-screen-capture-application-using-the-framebuffer/
http://stackoverflow.com/questions/12131429/android-get-screen-size-via-c
http://stackoverflow.com/questions/17304611/android-read-fb0-always-give-me-blackscreen